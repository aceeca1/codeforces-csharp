using System;

class B535 {
	public static void Main() {
		var s = Console.ReadLine();
		var a = new char[s.Length + 1];
		a[0] = '1';
		for (int i = 1; i <= s.Length; ++i)
			a[i] = s[i - 1] == '4' ? '0' : '1';
		var a1 = new string(a);
		Console.WriteLine(Convert.ToInt32(a1, 2) - 1);
	}
}
