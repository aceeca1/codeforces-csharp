using System;

class A219 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var a = new int[26];
        foreach (var i in s) ++a[i - 'a'];
        if (s.Length % n == 0 && Array.TrueForAll(a, k => k % n == 0)) {
            var b = new char[s.Length / n];
            int bz = 0;
            for (int i = 0; i < 26; ++i)
                for (int j = a[i] / n; j != 0; --j) b[bz++] = (char)('a' + i);
            for (int i = 0; i < n; ++i) Console.Write(b);
            Console.WriteLine();
        } else Console.WriteLine(-1);
    }
}
