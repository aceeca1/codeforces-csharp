using System;
using System.Linq;

class A255 {
    static string[] Name = {"chest", "biceps", "back"};
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[3];
        int k = 0;
        for (int i = 0; i < n; ++i) {
            b[k] += a[i];
            k = k == 2 ? 0 : k + 1;
        }
        Console.WriteLine(Name[Array.IndexOf(b, b.Max())]);
    }
}
