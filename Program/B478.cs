using System;

class B478 {
    static long f(long n) {
        return n * (n - 1) >> 1;
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]);
        var m = long.Parse(line[1]);
        var kMax = f(n - (m - 1));
        var m1 = n % m;
        var kMin = f(n / m + 1) * m1 + f(n / m) * (m - m1);
        Console.WriteLine($"{kMin} {kMax}");
    }
}
