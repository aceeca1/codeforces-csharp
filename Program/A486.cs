﻿using System;

class A486 {
    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine((n & 1) == 0 ? n >> 1 : -1 - (n >> 1));
    }
}
