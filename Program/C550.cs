﻿using System;

class C550 {
    class Div8 {
        class Success : Exception { }

        public string S;
        int Z;
        char[] A;

        void Put(int k, int no) {
            if (Z <= k || S.Length <= no) return;
            Put(k, no + 1);
            A[k] = S[no];
            var s = new string(A, 0, k + 1);
            if ((int.Parse(s) & 7) == 0) {
                S = s;
                throw new Success();
            }
            Put(k + 1, no + 1);
        }

        public Div8(string s, int z) {
            S = s;
            Z = z;
            A = new char[Z];
            try {
                Put(0, 0);
                S = null;
            } catch (Success) {}
        }
    }

    public static void Main() {
        var s = Console.ReadLine();
        if (s.IndexOf('0') != -1) s = "0";
        else s = new Div8(s, 3).S;
        if (s == null) Console.WriteLine("NO");
        else {
            Console.WriteLine("YES");
            Console.WriteLine(s);
        }
    }
}
