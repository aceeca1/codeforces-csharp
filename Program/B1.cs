using System;
using System.Collections.Generic;

class B1 {
    static bool IsRC(string s) {
        var c = s.IndexOf('C');
        if (c <= 0) return false;
        return char.IsDigit(s[c - 1]);
    }
    
    static string FromRC(string s) {
        var ss = s.Substring(1).Split('C');
        var r = int.Parse(ss[0]);
        var c = int.Parse(ss[1]);
        var a = new List<char>();
        while (c != 0) {
            int c0 = (c - 1) % 26;
            a.Add((char)('A' + c0));
            c = (c - (c0 + 1)) / 26;
        }
        a.Reverse();
        return string.Concat(a) + r;
    }
    
    static string ToRC(string s) {
        int k = 0;
        while (char.IsLetter(s[k])) ++k;
        var s0 = s.Substring(0, k);
        var s1 = s.Substring(k);
        int c = 0;
        foreach (var i in s0) c = c * 26 + (i - 'A' + 1);
        int r = int.Parse(s1);
        return $"R{r}C{c}";
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            Console.WriteLine(IsRC(s) ? FromRC(s) : ToRC(s));
        }
    }
}
