using System;

class A456 {
    static bool Sorted(int[] a) {
        for (int i = 1; i < a.Length; ++i)
            if (a[i - 1] > a[i]) return false;
        return true;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        var b = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            a[i] = int.Parse(line[0]);
            b[i] = int.Parse(line[1]);
        }
        Array.Sort(a, b);
        Console.WriteLine(Sorted(b) ? "Poor Alex" : "Happy Alex");
    }
}
