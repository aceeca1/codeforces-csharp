using System;

class B451 {
    class Interval {
        public int S, T;
    };
    
    static Interval HowToFlip(int[] a) {
        var k1 = 0;
        while (k1 < a.Length - 1 && a[k1] < a[k1 + 1]) ++k1;
        if (k1 == a.Length - 1) return new Interval { S = 0, T = 0 };
        var k2 = a.Length - 2;
        while (0 <= k2 && a[k2] < a[k2 + 1]) --k2;
        Array.Reverse(a, k1, k2 + 2 - k1);
        for (int i = 0; i < a.Length - 1; ++i)
            if (a[i + 1] < a[i]) return null;
        return new Interval { S = k1, T = k2 + 1 };
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var iv = HowToFlip(a);
        if (iv == null) Console.WriteLine("no");
        else {
            Console.WriteLine("yes");
            Console.WriteLine($"{iv.S + 1} {iv.T + 1}");
        }
    }
}
