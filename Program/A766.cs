using System;

class A766 {
    public static void Main() {
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        Console.WriteLine(s1 == s2 ? -1 : Math.Max(s1.Length, s2.Length));
    }
}
