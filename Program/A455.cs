﻿using System;
using System.Linq;

class A455 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new long[a.Max() + 1];
        foreach (var i in a) ++b[i];
        for (int i = 0; i < b.Length; ++i) b[i] *= i;
        b[1] = Math.Max(b[0], b[1]);
        for (int i = 2; i < b.Length; ++i)
            b[i] = Math.Max(b[i - 1], b[i] + b[i - 2]);
        Console.WriteLine(b[b.Length - 1]);
    }
}
