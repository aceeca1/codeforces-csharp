using System;

class A378 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        int c1 = 0, c2 = 0, c3 = 0;
        for (int i = 1; i <= 6; ++i) {
            var da = Math.Abs(a - i);
            var db = Math.Abs(b - i);
            if (da < db) ++c1;
            else if (da == db) ++c2;
            else ++c3;
        }
        Console.WriteLine($"{c1} {c2} {c3}");
    }
}
