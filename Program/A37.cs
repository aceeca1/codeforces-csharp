using System;
using System.Collections.Generic;
using System.Linq;

class A37 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new Dictionary<int, int>();
        foreach (var i in a) {
            int v;
            b.TryGetValue(i, out v);
            b[i] = v + 1;
        }
        Console.WriteLine($"{b.Values.Max()} {b.Count}");
    }
}
