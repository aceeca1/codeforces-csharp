using System;
using System.Linq;

class A146 {
    static bool Lucky(string s) {
        if (!s.All(k => k == '4' || k == '7')) return false;
        var n = s.Length >> 1;
        var sum1 = Enumerable.Range(0, n).Sum(k => s[k]);
        var sum2 = Enumerable.Range(n, n).Sum(k => s[k]);
        return sum1 == sum2;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        Console.WriteLine(Lucky(s) ? "YES" : "NO");
    }
}

