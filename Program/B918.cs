using System;
using System.Collections.Generic;

class B918 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var ipName = new Dictionary<string, string>();
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            ipName[line[1]] = line[0];
        }
        for (int i = 0; i < m; ++i) {
            var line1 = Console.ReadLine();
            line = line1.Split();
            var ip = line[1].Substring(0, line[1].Length - 1);
            Console.WriteLine($"{line1} #{ipName[ip]}");
        }
    }
}
