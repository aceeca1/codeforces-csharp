using System;
using System.Numerics;

class B617 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine().Replace(" ", "").Trim('0');
        if (s.Length == 0) Console.WriteLine(0);
        else {
            var answer = BigInteger.One;
            foreach (var i in s.Split('1')) answer *= i.Length + 1;
            Console.WriteLine(answer);
        }
    }
}
