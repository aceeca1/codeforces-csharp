using System;

class A731 {
    static int Distance(char c1, char c2) {
        int d = (c1 + 26 - c2) % 26;
        return d > 13 ? 26 - d : d;
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        char c = 'a';
        int ans = 0;
        foreach (var i in s) {
            ans += Distance(c, i);
            c = i;
        }
        Console.WriteLine(ans);
    }
}
