using System;

class B158 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[5];
        foreach (var i in Console.ReadLine().Split()) ++a[int.Parse(i)];
        var ans = a[4] + a[3] + ((a[2] + 1) >> 1);
        a[1] -= a[3] + ((a[2] & 1) << 1);
        if (a[1] < 0) a[1] = 0;
        ans += (a[1] + 3) >> 2;
        Console.WriteLine(ans);
    }
}
