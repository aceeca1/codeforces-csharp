using System;

class A124 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]);
        var b = int.Parse(line[2]);
        Console.WriteLine(Math.Min(n - a, b + 1));
    }
}
