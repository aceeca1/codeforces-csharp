using System;

class A527 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = long.Parse(line[0]);
        var b = long.Parse(line[1]);
        long ans = 0L;
        while (b != 0) {
            ans += a / b;
            var c = a % b;
            a = b;
            b = c;
        }
        Console.WriteLine(ans);
    }
}
