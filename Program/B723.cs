using System;
using System.Linq;

class B723 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var ss = s.Split('(', ')');
        int answer1 = 0, answer2 = 0;
        var opts = StringSplitOptions.RemoveEmptyEntries;
        for (int i = 0; i < ss.Length; i += 2) {
            var sss = ss[i].Split(new char[] { '_' }, opts);
            foreach (var j in sss)
                if (answer1 < j.Length) answer1 = j.Length;
        }
        for (int i = 1; i < ss.Length; i += 2) {
            var sss = ss[i].Split(new char[] { '_' }, opts);
            answer2 += sss.Length;
        }
        Console.WriteLine($"{answer1} {answer2}");
    }
}
