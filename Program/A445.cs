﻿using System;

class A445 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine().ToCharArray();
            for (int j = 0; j < m; ++j)
                if (s[j] == '.') s[j] = ((i + j) & 1) == 0 ? 'B' : 'W';
            Console.WriteLine(s);
        }
    }
}
