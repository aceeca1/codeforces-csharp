using System;

class B489 {
    public static void Main() {
        var n1 = int.Parse(Console.ReadLine());
        var a1 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var n2 = int.Parse(Console.ReadLine());
        var a2 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a1);
        Array.Sort(a2);
        int i1 = 0, i2 = 0, ans = 0;
        while (i1 < a1.Length && i2 < a2.Length) {
            var difference = a1[i1] - a2[i2];
            if (difference < -1) ++i1;
            else if (difference < 2) { ++i1; ++i2; ++ans; }
            else ++i2;
        }
        Console.WriteLine(ans);
    }
}
