using System;

class A459 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var x1 = int.Parse(line[0]);
        var y1 = int.Parse(line[1]);
        var x2 = int.Parse(line[2]);
        var y2 = int.Parse(line[3]);
        int x3 = 0, y3 = 0, x4 = 0, y4 = 0;
        bool hasSolution = true;
        if (x1 == x2) {
            x3 = x4 = x1 + (y1 - y2);
            y3 = y1;
            y4 = y2;
        } else if (y1 == y2) {
            y3 = y4 = y1 + (x1 - x2);
            x3 = x1;
            x4 = x2;
        } else if (Math.Abs(x1 - x2) != Math.Abs(y1 - y2)) {
            hasSolution = false;
        } else {
            x3 = x1;
            y3 = y2;
            x4 = x2;
            y4 = y1;
        }
        if (hasSolution) Console.WriteLine($"{x3} {y3} {x4} {y4}");
        else Console.WriteLine(-1);
    }
}
