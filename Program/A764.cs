using System;
using System.Numerics;

class A764 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var z = int.Parse(line[2]);
        var gcd = (int)BigInteger.GreatestCommonDivisor(n, m);
        Console.WriteLine(z / (n / gcd * m));
    }
}
