using System;

class A92 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        int k = 1;
        while (k <= m) {
            m -= k;
            if (k == n) k = 1; else ++k;
        }
        Console.WriteLine(m);
    }
}
