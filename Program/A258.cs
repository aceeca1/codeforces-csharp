using System;

class A258 {
	public static void Main() {
		var s = Console.ReadLine().ToCharArray();
		var zero = Array.IndexOf(s, '0');
		if (zero == -1) zero = s.Length - 1;
		for (int i = zero + 1; i < s.Length; ++i) s[i - 1] = s[i];
		Console.WriteLine(new string(s, 0, s.Length - 1));
	}
}
