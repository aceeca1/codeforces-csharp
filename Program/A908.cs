using System;

class A908 {
    public static void Main() {
        var s = Console.ReadLine();
        int answer = 0;
        foreach (var i in s)
            if ("aeiou13579".IndexOf(i) != -1) ++answer;
        Console.WriteLine(answer);
    }
}
