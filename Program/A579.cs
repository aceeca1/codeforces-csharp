using System;

class A579 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(BitCount.Int(n));
    }

    // Snippet: BitCount
    class BitCount {
        public static int Int(int n) {
            n = (n & 0x55555555) + ((n >> 1) & 0x55555555);
            n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
            n = (n & 0x0f0f0f0f) + ((n >> 4) & 0x0f0f0f0f);
            n = (n & 0x00ff00ff) + ((n >> 8) & 0x00ff00ff);
            return (n & 0xffff) + (n >> 16);
        }
    }
}
