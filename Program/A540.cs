using System;

class A540 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            var a1 = (s1[i] - s2[i] + 10) % 10;
            var a2 = (s2[i] - s1[i] + 10) % 10;
            ans += Math.Min(a1, a2);
        }
        Console.WriteLine(ans);
    }
}
