﻿using System;

class B764 {
    static void Swap(int[] a, int p1, int p2) {
        var t = a[p1];
        a[p1] = a[p2];
        a[p2] = t;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int p1 = 0, p2 = a.Length - 1;
        bool needSwap = true;
        while (p1 < p2) {
            if (needSwap) Swap(a, p1, p2);
            needSwap = !needSwap;
            ++p1;
            --p2;
        }
        Console.WriteLine(string.Join(" ", a));
    }
}
