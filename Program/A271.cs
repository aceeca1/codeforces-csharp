﻿using System;
using System.Linq;

class A271 {
    public static void Main() {
        var y = int.Parse(Console.ReadLine()) + 1;
        while (y.ToString().Distinct().Count() != 4) ++y;
        Console.WriteLine(y);
    }
}
