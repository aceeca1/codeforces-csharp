using System;

class A560 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(Array.IndexOf(a, 1) == -1 ? 1 : -1); 
    }
}
