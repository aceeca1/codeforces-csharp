using System;
using System.Collections.Generic;
using System.Linq;

class C659 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var m = int.Parse(line[1]);
        var a = new HashSet<int>(Console.ReadLine().Split().Select(int.Parse));
        var answer = new List<int>();
        for (int i = 1; i <= m; ++i)
            if (!a.Contains(i)) {
                m -= i;
                answer.Add(i);
            }
        Console.WriteLine(answer.Count);
        Console.WriteLine(string.Join(" ", answer));
    }
}
