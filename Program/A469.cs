﻿using System;
using System.Linq;

class A469 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new bool[n];
        for (int i = 0; i < 2; ++i)
            foreach (var j in Console.ReadLine().Split().Skip(1))
                a[int.Parse(j) - 1] = true;
        Console.WriteLine(a.All(k => k) 
            ? "I become the guy." 
            : "Oh, my keyboard!"
        );
    }
}
