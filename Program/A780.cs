﻿using System;

class A780 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new bool[n + 1];
        int sum = 0, max = 0;
        foreach (var i in a) {
            b[i] = !b[i];
            if (b[i]) ++sum; else --sum;
            if (max < sum) max = sum;
        }
        Console.WriteLine(max);
    }
}
