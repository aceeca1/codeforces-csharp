using System;
using System.Linq;

class A676 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var a1 = Array.IndexOf(a, 1);
        var an = Array.IndexOf(a, n);
        Console.WriteLine(new [] {a1, n - 1 - a1, an, n - 1 - an}.Max());
    }
}
