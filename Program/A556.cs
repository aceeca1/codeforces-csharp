using System;
using System.Linq;

class A556 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var s0 = s.Count(k => k == '0');
        var s1 = s.Length - s0;
        Console.WriteLine(Math.Abs(s0 - s1));
    }
}
