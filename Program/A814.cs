using System;

class A814 {
    static bool IsIncreasing(int[] a) {
        for (int i = 1; i < a.Length; ++i)
            if (a[i] <= a[i - 1]) return false;
        return true;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n1 = int.Parse(line[0]);
        var n2 = int.Parse(line[1]);
        var a1 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var a2 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a2);
        int p = a2.Length - 1;
        for (int i = 0; i < n1; ++i)
            if (a1[i] == 0) a1[i] = a2[p--];
        Console.WriteLine(IsIncreasing(a1) ? "No" : "Yes");
    }
}
