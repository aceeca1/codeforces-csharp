using System;
using System.Collections.Generic;

class B118 {
    static IEnumerable<char> Line(int n, int i) {
        for (int j = -n; j <= n; ++j) {
            var c = n - Math.Abs(i) - Math.Abs(j);
            yield return c < 0 ? ' ' : (char)('0' + c);
        }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = -n; i <= n; ++i) 
            Console.WriteLine(string.Join(" ", Line(n, i)).TrimEnd());
    }
}
