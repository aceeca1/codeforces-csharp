﻿using System;

class A404 {
    static bool Check(string[] a) {
        var cX = a[0][0];
        var cY = a[0][1];
        if (cX == cY) return false;
        for (int i = 0; i < a.Length; ++i)
            for (int j = 0; j < a.Length; ++j) {
                var c = i == j || i + j == a.Length - 1 ? cX : cY;
                if (c != a[i][j]) return false;
            }
        return true;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        Console.WriteLine(Check(a) ? "YES" : "NO");
    }
}
