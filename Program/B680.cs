﻿using System;

class B680 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]) - 1;
        var t = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n];
        var c = new int[n];
        for (int i = 0; i < n; ++i) {
            var ia = Math.Abs(i - a);
            if (t[i] != 0) ++b[ia];
            ++c[ia];
        }
        int answer = 0;
        for (int i = 0; i < n; ++i) 
            if (b[i] == c[i]) answer += b[i];
        Console.WriteLine(answer);
    }
}
