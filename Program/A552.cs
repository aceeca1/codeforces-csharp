﻿using System;

class A552 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int answer = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var x1 = int.Parse(line[0]);
            var y1 = int.Parse(line[1]);
            var x2 = int.Parse(line[2]);
            var y2 = int.Parse(line[3]);
            answer += (x2 - x1 + 1) * (y2 - y1 + 1);
        }
        Console.WriteLine(answer);
    }
}
