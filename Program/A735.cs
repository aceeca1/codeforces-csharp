﻿using System;

class A735 {
    static bool Go(string s, int pT, int pG, int k) {
        while (true) {
            if (pG < pT) return false;
            if (pT == pG) return true;
            if (s[pT] == '#') return false;
            pT += k;
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var s = Console.ReadLine();
        var pT = s.IndexOf('T');
        var pG = s.IndexOf('G');
        var ans = pT < pG ? Go(s, pT, pG, k) : Go(s, pG, pT, k);
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
