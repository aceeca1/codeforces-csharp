using System;
using System.Linq;

class A822 {
    public static void Main() {
        var n = Console.ReadLine().Split().Select(int.Parse).Min();
        int ans = 1;
        for (int i = 2; i <= n; ++i) ans *= i;
        Console.WriteLine(ans);
    }
}
