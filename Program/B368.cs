using System;
using System.Collections.Generic;

class B368 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new HashSet<int>();
        for (int i = n - 1; i >= 0; --i) {
            b.Add(a[i]);
            a[i] = b.Count;
        }
        for (int i = 0; i < m; ++i)
            Console.WriteLine(a[int.Parse(Console.ReadLine()) - 1]);
    }
}
