using System;
using System.Linq;

class A705 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(String.Join(" that ", Enumerable.Range(0, n).Select(
            k => (k & 1) == 0 ? "I hate" : "I love"
        )) + " it");
    }
}
