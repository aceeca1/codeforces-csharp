using System;

class C489 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var m = int.Parse(line[0]);
        var s = int.Parse(line[1]);
        if (s == 0) {
            if (m == 1) Console.WriteLine("0 0");
            else Console.WriteLine("-1 -1");
        } else if (m * 9 < s) Console.WriteLine("-1 -1");
        else {
            var a = new char[m];
            a[0] = '1';
            for (int i = 1; i < m; ++i) a[i] = '0';
            int s1 = s - 1;
            for (int i = m - 1; i >= 0; --i) {
                int delta = Math.Min(s1, '9' - a[i]);
                s1 -= delta;
                a[i] += (char)delta;
            }
            var num1 = new string(a);
            for (int i = 0; i < m; ++i) a[i] = '0';
            int s2 = s;
            for (int i = 0; i < m; ++i) {
                int delta = Math.Min(s2, '9' - a[i]);
                s2 -= delta;
                a[i] += (char)delta;
            }
            var num2 = new string(a);
            Console.WriteLine($"{num1} {num2}");
        }
    }
}
