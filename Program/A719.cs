using System;

class A719 {
    static string Next(int[] a) {
        switch (a[a.Length - 1]) {
            case 0: return "UP";
            case 15: return "DOWN";
            default:
                if (a.Length == 1) return "-1";
                else if (a[a.Length - 2] < a[a.Length - 1]) return "UP";
                else return "DOWN";
        }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(Next(a));
    }
}
