using System;

class A158 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]) - 1;
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        while (k + 1 < a.Length && a[k + 1] == a[k]) ++k;
        while (0 <= k && a[k] == 0) --k;
        Console.WriteLine(k + 1);
    }
}
