using System;

class B805 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new char[n];
        for (int i = 0; i < n; ++i) a[i] = "aabb"[i & 3];
        Console.WriteLine(a);
    }
}
