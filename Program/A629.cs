using System;
using System.Linq;

class A629 {
    static int Pairs(char[][] a) {
        return a.Sum(k => {
            var c = k.Count(k1 => k1 == 'C');
            return c * (c - 1) >> 1;
        });
    }

    static char[][] Transpose(char[][] a) {
        var answer = new char[a[0].Length][];
        for (int i = 0; i < a[0].Length; ++i) answer[i] = new char[a.Length];
        for (int i = 0; i < a.Length; ++i)
            for (int j = 0; j < a[0].Length; ++j)
                answer[j][i] = a[i][j];
        return answer;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new char[n][];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine().ToCharArray();
        Console.WriteLine(Pairs(a) + Pairs(Transpose(a)));
    }
}
