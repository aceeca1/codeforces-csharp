using System;

class A567 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 0; i < n; ++i) {
            var minL = i != 0 ? a[i] - a[i - 1] : int.MaxValue;
            var minR = i != n - 1 ? a[i + 1] - a[i] : int.MaxValue;
            var min = Math.Min(minL, minR);
            var maxL = a[i] - a[0];
            var maxR = a[n - 1] - a[i];
            var max = Math.Max(maxL, maxR);
            Console.WriteLine($"{min} {max}");
        }
    }
}
