using System;
using System.Collections.Generic;

class A118 {
    static IEnumerable<char> Process(string s) {
        foreach (var i in s) {
            var ii = char.ToLower(i);
            switch (ii) {
                case 'a': case 'o': case 'y':
                case 'e': case 'u': case 'i': break;
                default: 
                    yield return '.';
                    yield return ii;
                    break;
            }
        }
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(String.Concat(Process(s)));
    }
}
