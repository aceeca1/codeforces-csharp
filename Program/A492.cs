﻿using System;

class A492 {
    public static void Main() {
        var m = int.Parse(Console.ReadLine()) * 6;
        var k = (int)Math.Floor(Math.Pow(m, 1.0 / 3.0));
        while (k * (k + 1) * (k + 2) <= m) ++k;
        Console.WriteLine(k - 1);
    }
}
