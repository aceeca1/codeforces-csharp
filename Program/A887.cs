using System;
using System.Linq;

class A887 {
    public static void Main() {
        var s = Console.ReadLine();
        var zero = s.SkipWhile(k => k == '0').Count(k => k == '0');
        Console.WriteLine(6 <= zero ? "yes" : "no");
    }
}
