﻿using System;

class A41 {
    public static void Main() {
        var s1 = Console.ReadLine().ToCharArray();
        Array.Reverse(s1);
        var s2 = Console.ReadLine();
        Console.WriteLine(new string(s1) == s2 ? "YES" : "NO");
    }
}
