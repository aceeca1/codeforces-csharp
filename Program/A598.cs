using System;

class A598 {
    static int f(int n) {
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        return n;
    }
    
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            var n = int.Parse(Console.ReadLine());
            Console.WriteLine(((long)n * (n + 1) >> 1) - (f(n) << 1));
        }
    }
}
