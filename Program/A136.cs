﻿using System;

class A136 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n];
        for (int i = 0; i < n; ++i) b[a[i] - 1] = i + 1;
        Console.WriteLine(string.Join(" ", b));
    }
}
