﻿using System;

class A711 {
    static bool Seated(string[] a) {
        for (int i = 0; i < a.Length; ++i)
            if (a[i].Contains("OO")) {
                if (a[i].Substring(0, 2) == "OO")
                    a[i] = "++" + a[i].Substring(2);
                else a[i] = a[i].Replace("OO", "++");
                return true;
            }
        return false;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        if (Seated(a)) {
            Console.WriteLine("YES");
            for (int i = 0; i < n; ++i) Console.WriteLine(a[i]);
        } else Console.WriteLine("NO");
    }
}
