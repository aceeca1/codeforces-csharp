﻿using System;
using System.Collections.Generic;
using System.Linq;

class A479 {
    static IEnumerable<int> AllExpression(int a, int b, int c) {
        yield return a + b + c;
        yield return a + b * c;
        yield return a * b + c;
        yield return a * b * c;
        yield return (a + b) * c;
        yield return a * (b + c);
    }

    public static void Main() {
        var a = int.Parse(Console.ReadLine());
        var b = int.Parse(Console.ReadLine());
        var c = int.Parse(Console.ReadLine());
        Console.WriteLine(AllExpression(a, b, c).Max());
    }
}
