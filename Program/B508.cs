using System;

class B508 {
    public static void Main() {
        var s = Console.ReadLine().ToCharArray();
        var k1 = Array.FindIndex(s, k => (k & 1) == 0 && k < s[s.Length - 1]);
        if (k1 == -1) k1 = Array.FindLastIndex(s, k => (k & 1) == 0);
        if (k1 == -1) Console.WriteLine(-1); 
        else {
            var t = s[k1];
            s[k1] = s[s.Length - 1];
            s[s.Length - 1] = t;
            Console.WriteLine(new string(s));
        }
    }
}
