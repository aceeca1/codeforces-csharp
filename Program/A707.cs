﻿using System;
using System.Linq;

class A707 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(Enumerable.Range(0, n).Any(k => 
            Console.ReadLine().Any(k1 => 
                k1 == 'C' || k1 == 'M' || k1 == 'Y'
            )
        ) ? "#Color" : "#Black&White");        
    }
}
