using System;
using System.Globalization;

class A919 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        double minPrice = Double.PositiveInfinity;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var a = int.Parse(line[0]);
            var b = int.Parse(line[1]);
            var price = (double)a / b;
            if (price < minPrice) minPrice = price;
        }
        var answer = minPrice * m;
        Console.WriteLine(answer.ToString(CultureInfo.InvariantCulture));
    }
}
