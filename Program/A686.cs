using System;

class A686 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = long.Parse(line[1]);
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var op = line[0][0];
            var num = long.Parse(line[1]);
            switch (op) {
                case '+': x += num; break;
                case '-': if (num <= x) x -= num; else ++ans; break;
            }
        }
        Console.WriteLine($"{x} {ans}");
    }
}
