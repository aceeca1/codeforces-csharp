using System;

class A385 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var c = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int max = 0;
        for (int i = 1; i < a.Length; ++i) 
            max = Math.Max(max, a[i - 1] - a[i]);
        Console.WriteLine(Math.Max(max - c, 0));
    }
}
