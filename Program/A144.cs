﻿using System;
using System.Linq;

class A144 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var min = a.Min();
        var max = a.Max();
        var argmin = Array.LastIndexOf(a, min);
        var argmax = Array.IndexOf(a, max);
        var ans = argmax + (a.Length - 1 - argmin);
        if (argmin < argmax) --ans;
        Console.WriteLine(ans);
    }
}
