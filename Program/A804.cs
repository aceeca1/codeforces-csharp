using System;

class A804 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((n - 1) >> 1);
    }
}
