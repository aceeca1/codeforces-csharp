using System;
using System.Collections.Generic;
using System.Linq;

class A721 {
    class PartitionBy: APartitionBy<char, PartitionBy.Combiner> {
        public class Combiner: ICombiner {
            public char Data;
            public int Amount;
            
            public void Add(char data) { ++Amount; Data = data; }
            public Combiner Result() => this;
        }
        
        protected override IEnumerable<char> GetInput() => Input;
        protected override ICombiner MakeCombiner() => new Combiner();
        protected override bool IsEquivalent(char a1, char a2) => a1 == a2;
        
        public IEnumerable<char> Input;
        public PartitionBy(IEnumerable<char> input) { Input = input; }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var ans = new List<int>(
            new PartitionBy(s).Result()
            .Where(k => k.Data == 'B').Select(k => k.Amount)
        );
        Console.WriteLine(ans.Count);
        if (ans.Count != 0) Console.WriteLine(string.Join(" ", ans));
    }

    // Snippet: PartitionBy
    abstract class APartitionBy<T, U> {
        public interface ICombiner {
            void Add(T data);
            U Result();
        }
    
        protected abstract IEnumerable<T> GetInput();
        protected abstract bool IsEquivalent(T a1, T a2);
        protected abstract ICombiner MakeCombiner();
        
        public IEnumerable<U> Result() {
            var data = default(T);
            var combiner = default(ICombiner);
            foreach (var i in GetInput())
                if (combiner == null) {
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                } else if (IsEquivalent(data, i)) {
                    data = i;
                    combiner.Add(i);
                } else {
                    yield return combiner.Result();
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                }
            if (combiner != null) yield return combiner.Result();
        }
    }
}
