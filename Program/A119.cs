﻿using System;
using System.Numerics;

class A119 {
    static bool Simon(int a, int b, int n) {
        for (;;) {
            if (n == 0) return false;
            n -= (int)BigInteger.GreatestCommonDivisor(n, a);
            if (n == 0) return true;
            n -= (int)BigInteger.GreatestCommonDivisor(n, b);
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var n = int.Parse(line[2]);
        Console.WriteLine(Simon(a, b, n) ? '0' : '1');
    }
}
