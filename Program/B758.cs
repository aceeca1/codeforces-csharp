using System;

class B758 {
    public static void Main() {
        var s = Console.ReadLine();
        var a = new int[4];
        var c = new char[4];
        for (int i = 0; i < s.Length; ++i)
            if (s[i] == '!') ++a[i & 3];
            else c[i & 3] = s[i];
        Array.Sort(c, a);
        Console.WriteLine($"{a[2]} {a[0]} {a[3]} {a[1]}");
    }
}
