using System;

class A278 {
    static int Distance(int[] a, int s, int t) {
        int answer = 0;
        for (int i = s; i != t; i = i == a.Length - 1 ? 0 : i + 1)
            answer += a[i];
        return answer;
    }

    public static void Main() {
        Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var line = Console.ReadLine().Split();
        var s = int.Parse(line[0]) - 1;
        var t = int.Parse(line[1]) - 1;
        Console.WriteLine(Math.Min(Distance(a, s, t), Distance(a, t, s)));
    }
}
