﻿using System;

class C545 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var x = new int[n];
        var h = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            x[i] = int.Parse(line[0]);
            h[i] = int.Parse(line[1]);
        }
        int last = int.MinValue;
        int answer = 0;
        for (int i = 0; i < n; ++i) {
            int next = i == n - 1 ? int.MaxValue : x[i + 1];
            if (last < x[i] - h[i]) { ++answer; last = x[i]; }
            else if (x[i] + h[i] < next) { ++answer; last = x[i] + h[i]; }
            else { last = x[i]; }
        }
        Console.WriteLine(answer);
    }
}
