using System;
using System.Linq;

class B459 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int max = a.Max(), min = a.Min();
        var ans1 = max - min;
        var ans2 = max == min 
            ? (long)n * (n - 1) >> 1 
            : (long)a.Count(k => k == max) * a.Count(k => k == min);
        Console.WriteLine($"{ans1} {ans2}");
    }
}
