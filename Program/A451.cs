﻿using System;
using System.Linq;

class A451 {
    public static void Main() {
        var min = Console.ReadLine().Split().Min(int.Parse);
        Console.WriteLine((min & 1) == 0 ? "Malvika" : "Akshat");
    }
}
