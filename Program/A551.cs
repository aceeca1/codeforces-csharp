using System;

class A551 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = a.Clone() as int[];
        Array.Sort(b);
        for (int i = 0; i < n; ++i)
            a[i] = n - BSearch.Int(0, n, k => a[i] < b[k]) + 1;
        Console.WriteLine(string.Join(" ", a));
    }
    
    // Snippet: BSearch
    class BSearch {
        public static int Int(int s, int t, Func<int, bool> f) {
            for (;;) {
                if (s == t) return s;
                int m = s + ((t - s) >> 1);
                if (f(m)) t = m; else s = m + 1;
            }
        }
    }
}
