using System;

class A659 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]);
        var b = int.Parse(line[2]);
        Console.WriteLine(((a + b) % n + n + n - 1) % n + 1);
    }
}
