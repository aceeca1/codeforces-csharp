using System;
using System.Collections.Generic;
using System.Linq;

class B538 {
	public static IEnumerable<string> Solve(string s) {
		for (int i = s.Max(); i != '0'; --i) {
			var ss = s.Select(k => k < i ? '0' : '1').ToArray();
			var k1 = Array.IndexOf(ss, '1');
			if (k1 == -1) k1 = ss.Length - 1;
			yield return new string(ss, k1, ss.Length - k1);
		}
	}

	public static void Main() {
		var s = Console.ReadLine();
		Console.WriteLine((char)s.Max());
		Console.WriteLine(string.Join(" ", Solve(s)));
	}
}
