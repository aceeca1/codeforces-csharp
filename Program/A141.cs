﻿using System;
using System.Linq;

class A141 {
    public static void Main() {
        var a = new int[256];
        foreach (var i in new int[] { 1, 1, -1 })
            foreach (var j in Console.ReadLine()) a[j] += i;
        Console.WriteLine(a.All(k => k == 0) ? "YES" : "NO");
    }
}
