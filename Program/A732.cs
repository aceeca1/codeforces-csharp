using System;

class A732 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[0]);
        var r = int.Parse(line[1]);
        int a = k;
        while (a % 10 != 0 && a % 10 != r) a += k;
        Console.WriteLine(a / k);
    }
}
