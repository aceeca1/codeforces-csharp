﻿using System;

class B750 {
    static bool ReadJudge(int n) {
        int a = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var num = int.Parse(line[0]);
            switch (line[1]) {
                case "South":
                    a += num;
                    if (20000 < a) return false;
                    break;
                case "North":
                    a -= num;
                    if (a < 0) return false;
                    break;
                default:
                    if (a == 0 || a == 20000) return false;
                    break;
            }
        }
        return a == 0;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(ReadJudge(n) ? "YES" : "NO");
    }
}
