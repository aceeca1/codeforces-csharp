using System;
using System.Linq;

class A448 {
    public static void Main() {
        var a = Console.ReadLine().Split().Sum(int.Parse);
        var b = Console.ReadLine().Split().Sum(int.Parse);
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((a + 4) / 5 + (b + 9) / 10 <= n ? "YES" : "NO");
    }
}
