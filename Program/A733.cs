using System;
using System.Collections.Generic;
using System.Linq;

class A733 {
    class PartitionBy: APartitionBy<char, int> {
        class Combiner: ICombiner {
            int amount;
            public void Add(char data) { ++amount; }
            public int Result() => amount;
        }
        
        protected override IEnumerable<char> GetInput() => Input;
        protected override ICombiner MakeCombiner() => new Combiner();
        protected override bool IsEquivalent(char a1, char a2) {
            switch (a1) {
                case 'A': case 'E': case 'I': 
                case 'O': case 'U': case 'Y': return false;
                default: return true;
            }
        }
        
        public IEnumerable<char> Input;
        public PartitionBy(IEnumerable<char> input) { Input = input; }
    }
    
    public static void Main() {
        var s = Console.ReadLine() + 'A';
        Console.WriteLine(new PartitionBy(s).Result().Max());
    }
    
    // Snippet: PartitionBy
    abstract class APartitionBy<T, U> {
        public interface ICombiner {
            void Add(T data);
            U Result();
        }
    
        protected abstract IEnumerable<T> GetInput();
        protected abstract bool IsEquivalent(T a1, T a2);
        protected abstract ICombiner MakeCombiner();
        
        public IEnumerable<U> Result() {
            var data = default(T);
            var combiner = default(ICombiner);
            foreach (var i in GetInput())
                if (combiner == null) {
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                } else if (IsEquivalent(data, i)) {
                    data = i;
                    combiner.Add(i);
                } else {
                    yield return combiner.Result();
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                }
            if (combiner != null) yield return combiner.Result();
        }
    }
}
