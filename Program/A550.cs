using System;

class A550 {
    static bool Find(string s, string s1, string s2) {
        var pos1 = s.IndexOf(s1);
        if (pos1 == -1) return false;
        return s.IndexOf(s2, pos1 + s1.Length) != -1;
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(
            Find(s, "AB", "BA") || Find(s, "BA", "AB") ? "YES" : "NO"
        );
    }
}
