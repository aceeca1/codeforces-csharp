using System;
using System.Linq;

class A507 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n];
        for (int i = 0; i < n; ++i) b[i] = i + 1;
        Array.Sort(a, b);
        int ans = 0;
        while (ans < n && a[ans] <= k) k -= a[ans++];
        Console.WriteLine(ans);
        if (ans != 0) Console.WriteLine(string.Join(" ", b.Take(ans)));
    }
}
