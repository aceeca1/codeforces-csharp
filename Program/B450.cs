using System;

class B450 {
    const int M = 1000000007;

    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = (int.Parse(line[0]) + M) % M;
        var a2 = (int.Parse(line[1]) + M) % M;
        var n = (int.Parse(Console.ReadLine()) + 5) % 6 + 1;
        while (n != 1) {
            var a3 = (a2 + M - a1) % M;
            a1 = a2;
            a2 = a3;
            --n;
        }
        Console.WriteLine(a1);
    }
}
