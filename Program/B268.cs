using System;

class B268 {
    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine(n * (n * n + 5) / 6);
    }
}
