using System;

class A270 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine(360 % (180 - a) == 0 ? "YES" : "NO");
        }
    }
}
