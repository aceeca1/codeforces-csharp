using System;

class A742 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(n == 0 ? '1' : "6842"[n & 3]);
    }
}
