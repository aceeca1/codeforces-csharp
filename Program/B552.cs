using System;

class B552 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int lb = 1, ub = 9, digit = 1;
        long ans = 0;
        while (ub < n) {
            ans += (long)digit * (ub - lb + 1);
            lb *= 10;
            ub = lb * 10 - 1;
            ++digit;
        }
        ans += (long)digit * (n - lb + 1);
        Console.WriteLine(ans);
    }
}
