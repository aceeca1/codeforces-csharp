using System;

class B581 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int max = -1;
        for (int i = n - 1; i >= 0; --i) {
            var v = Math.Max(0, max + 1 - a[i]);
            if (max < a[i]) max = a[i];
            a[i] = v;
        }
        Console.WriteLine(string.Join(" ", a));
    }
}
