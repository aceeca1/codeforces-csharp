﻿using System;
using System.Linq;

class A38 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var d = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        Console.WriteLine(Enumerable.Range(a - 1, b - a).Sum(k => d[k]));
    }
}
