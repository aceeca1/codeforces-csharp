using System;

class A651 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var a2 = int.Parse(line[1]);
        var ans = (a1 - a2) % 3 == 0 ? a1 + a2 - 3 : a1 + a2 - 2;
        Console.WriteLine(Math.Max(0, ans));
    }
}
