using System;

class B439 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = long.Parse(line[1]);
        var c = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(c);
        long answer = 0;
        foreach (var i in c) {
            answer += i * x;
            if (x > 1) --x;
        }
        Console.WriteLine(answer);
    }
}
