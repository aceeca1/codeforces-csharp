using System;

class B766 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        bool ans = false;
        for (int i = 0; i < a.Length - 2; ++i)
            if (a[i + 2] < a[i] + a[i + 1]) { ans = true; break; }
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
