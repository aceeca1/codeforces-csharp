using System;
using System.Linq;

class A698 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n, 3];
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < 3; ++j) {
                b[i, j] = int.MaxValue >> 1;
                if ((j & ~a[i]) != 0) continue;
                for (int k = 0; k < 3; ++k)
                    if (k == 0 || k != j) {
                        var u = i == 0 ? 0 : b[i - 1, k];
                        if (j == 0) ++u;
                        if (u < b[i, j]) b[i, j] = u;
                    }
            }
        Console.WriteLine(Enumerable.Range(0, 3).Min(k => b[n - 1, k]));
    }
}
