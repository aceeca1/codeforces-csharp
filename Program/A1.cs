using System;

class A1 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]);
        var m = long.Parse(line[1]);
        var a = long.Parse(line[2]);
        var n1 = (n + a - 1) / a;
        var m1 = (m + a - 1) / a;
        Console.WriteLine(n1 * m1);
    }
}
