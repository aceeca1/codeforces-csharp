using System;

class A515 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var s = int.Parse(line[2]);
        Console.WriteLine(
            ((a ^ b ^ s) & 1) == 0 && Math.Abs(a) + Math.Abs(b) <= s
                ? "Yes" : "No"
        );
    }
}
