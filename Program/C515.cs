using System;
using System.Linq;

class C515 {
    static string[] A = { 
        "", "", "2", "3", "223", "5", "35", "7", "2227", "2337" 
    };
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Console.ReadLine().SelectMany(k => A[k - '0']);
        Console.WriteLine(string.Concat(a.OrderByDescending(k => k)));
    }
}
