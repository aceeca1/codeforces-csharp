using System;

class A281 {
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(char.ToUpper(s[0]) + s.Substring(1));
    }
}
