using System;

class A757 {
    const string Bulbasaur = "Bulbasaur";
    
    public static void Main() {
        var a = new int[256];
        foreach (var i in Console.ReadLine()) ++a[i];
        var b = new int[a.Length];
        foreach (var i in Bulbasaur) ++b[i];
        int min = int.MaxValue;
        for (int i = 0; i < a.Length; ++i)
            if (b[i] != 0) min = Math.Min(min, a[i] / b[i]);
        Console.WriteLine(min);
    }
}
