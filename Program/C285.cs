using System;

class C285 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        long answer = 0;
        for (int i = 0; i < a.Length; ++i)
            answer += Math.Abs(a[i] - (i + 1));
        Console.WriteLine(answer);
    }
}
