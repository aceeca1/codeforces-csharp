using System;

class B580 {
    class Person {
        public int M, S;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = int.Parse(line[1]);
        var a = new Person[n];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            a[i] = new Person {
                M = int.Parse(line[0]),
                S = int.Parse(line[1])
            };
        }
        Array.Sort(a, (a1, a2) => a1.M.CompareTo(a2.M));
        int p1 = 0, p2 = 0;
        long curr = 0, ans = 0;
        while (p2 < a.Length) {
            if (a[p2].M - a[p1].M < d) curr += a[p2++].S;
            else curr -= a[p1++].S;
            if (ans < curr) ans = curr;
        }
        Console.WriteLine(ans);
    }
}
