using System;
using System.Collections.Generic;

class A583 {
    static IEnumerable<int> Solve() {
        var n = int.Parse(Console.ReadLine());
        var m = n * n;
        var a0 = new bool[n];
        var a1 = new bool[n];
        for (int i = 0; i < m; ++i) {
            var line = Console.ReadLine().Split();
            var c0 = int.Parse(line[0]) - 1;
            var c1 = int.Parse(line[1]) - 1;
            if (a0[c0] || a1[c1]) continue;
            a0[c0] = true;
            a1[c1] = true;
            yield return i + 1;
        }
    }

    public static void Main() {
        Console.WriteLine(string.Join(" ", Solve()));
    }
}
