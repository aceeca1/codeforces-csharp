﻿using System;

class A460 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var day = 1;
        while (n != 0) if (day++ % m != 0) --n;
        Console.WriteLine(day - 1);
    }
}
