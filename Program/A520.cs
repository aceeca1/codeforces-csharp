﻿using System;
using System.Linq;

class A520 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new bool[26];
        foreach (var i in Console.ReadLine()) a[char.ToLower(i) - 'a'] = true;
        Console.WriteLine(a.All(k => k) ? "YES" : "NO");
    }
}
