using System;

class A501 {
    static double CalcPoint(int p, int t) {
        return Math.Max(0.3 * p, p - p * t * 0.004);
    }
    
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var misha = CalcPoint(a[0], a[2]);
        var vasya = CalcPoint(a[1], a[3]);
        Console.WriteLine(
            misha == vasya ? "Tie" :
            misha < vasya ? "Vasya" : "Misha"
        );
    }
}
