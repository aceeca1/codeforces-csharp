﻿using System;

class A82 {
    static string[] Name = {
        "Sheldon", "Leonard", "Penny", "Rajesh", "Howard"
    };

    public static void Main() {
        var n = int.Parse(Console.ReadLine()) - 1;
        var k = (int)Math.Floor(Math.Log(n * 0.2 + 1) / Math.Log(2.0));
        Console.WriteLine(Name[(n - 5 * ((1 << k) - 1)) >> k]);
    }
}
