using System;

class A573 {
    static int Reduce(int n) {
        n /= n & -n;
        while (n % 3 == 0) n /= 3;
        return n;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 0; i < a.Length; ++i) a[i] = Reduce(a[i]);
        Console.WriteLine(Array.TrueForAll(a, k => k == a[0]) ? "Yes" : "No");
    }
}
