using System;

class A189 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = new int[] {
            int.Parse(line[1]),
            int.Parse(line[2]),
            int.Parse(line[3])
        };
        var b = new int[n + 1];
        for (int i = 1; i <= n; ++i) b[i] = int.MinValue;
        foreach (var i in a)
            for (int j = i; j <= n; ++j)
                b[j] = Math.Max(b[j], b[j - i] + 1);
        Console.WriteLine(b[n]);
    }
}
