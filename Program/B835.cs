using System;
using System.Linq;

class B835 {
    public static void Main() {
        var k = int.Parse(Console.ReadLine());
        var n = Console.ReadLine().ToCharArray();
        Array.Sort(n);
        var s = n.Sum(k1 => k1 - '0');
        int i = 0;
        while (s < k) {
            s += '9' - n[i];
            n[i++] = '9';
        }
        Console.WriteLine(i);
    }
}
