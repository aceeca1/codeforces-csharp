using System;
using System.Linq;

class A330 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var r = int.Parse(line[0]);
        var c = int.Parse(line[1]);
        var ar = new bool[r];
        var ac = new bool[c];
        for (int i = 0; i < r; ++i) {
            var line1 = Console.ReadLine();
            for (int j = 0; j < c; ++j)
                if (line1[j] == 'S') ar[i] = ac[j] = true;
        }
        Console.WriteLine(r * c - ar.Count(k => k) * ac.Count(k => k));
    }
}
