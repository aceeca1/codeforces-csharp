using System;

class A900 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int a0 = 0, a1 = 0;
        for (int i = 0; i < n; ++i)
            if (int.Parse(Console.ReadLine().Split()[0]) < 0) ++a0;
            else ++a1;
        Console.WriteLine(Math.Min(a0, a1) <= 1 ? "Yes" : "No");
    }
}
