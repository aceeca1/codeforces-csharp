﻿using System;

class A499 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = int.Parse(line[1]);
        var l = new int[n];
        var r = new int[n];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            l[i] = int.Parse(line[0]);
            r[i] = int.Parse(line[1]);
        }
        int k = 1, answer = 0;
        for (int i = 0; i < n; ++i) {
            while (k + x <= l[i]) k += x;
            while (k <= r[i]) { ++k; ++answer; }
        }
        Console.WriteLine(answer);
    }
}
