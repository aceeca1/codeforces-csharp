using System;

class A832 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]);
        var k = long.Parse(line[1]);
        Console.WriteLine(((n / k) & 1) == 0 ? "NO" : "YES");
    }
}
