using System;

class A71 {
    static string Abbreviate(string s) {
        if (s.Length <= 10) return s;
        return s[0] + (s.Length - 2).ToString() + s[s.Length - 1];
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            Console.WriteLine(Abbreviate(s));
        }
    }
}
