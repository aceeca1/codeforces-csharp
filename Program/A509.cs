﻿using System;

class A509 {
    // Snippet: Choose
    static T Choose<T>(int n, int k, IOps<T> ops) {
        var ans = ops.One();
        for (int i = 1; i <= k; ++i)
            ans = ops.Div(ops.Mul(ans, ops.From(n + 1 - i)), ops.From(i));
        return ans;
    }

    public static void Main() {
        var m = int.Parse(Console.ReadLine()) - 1;
        Console.WriteLine(Choose(m + m, m, new OpsInt()));
    }

    // Snippet: IOps
    interface IOps<T> {
        T From(int a);
        T Zero();
        T One();
        T MinusOne();
        T MinValue();
        T MaxValue();

        bool IsEven(T a);
        bool IsOne(T a);
        bool IsZero(T a);
        int Sign(T a);

        T Inc(T a);
        T Dec(T a);
        T Abs(T a);
        T Add(T a1, T a2);
        T Sub(T a1, T a2);
        T Mul(T a1, T a2);
        T Div(T a1, T a2);
        T Mod(T a1, T a2);
        T Shl(T a1, int a2);
        T Shr(T a1, int a2);
        T Max(T a1, T a2);
        T Min(T a1, T a2);

        bool Less(T a1, T a2);
        bool Eq(T a1, T a2);
    }

    // Snippet: OpsInt
    class OpsInt : IOps<int> {
        public int From(int a) { return a; }
        public int Zero() { return 0; }
        public int One() { return 1; }
        public int MinusOne() { return -1; }
        public int MinValue() { return int.MinValue; }
        public int MaxValue() { return int.MaxValue; }

        public bool IsEven(int a) { return (a & 1) == 0; }
        public bool IsOne(int a) { return a == 1; }
        public bool IsZero(int a) { return a == 0; }
        public int Sign(int a) { return a; }

        public int Inc(int a) { return a + 1; }
        public int Dec(int a) { return a - 1; }
        public int Abs(int a) { return Math.Abs(a); }
        public int Add(int a1, int a2) { return a1 + a2; }
        public int Sub(int a1, int a2) { return a1 - a2; }
        public int Mul(int a1, int a2) { return a1 * a2; }
        public int Div(int a1, int a2) { return a1 / a2; }
        public int Mod(int a1, int a2) { return a1 % a2; }
        public int Shl(int a1, int a2) { return a1 << a2; }
        public int Shr(int a1, int a2) { return a2 >> a2; }
        public int Max(int a1, int a2) { return Math.Max(a1, a2); }
        public int Min(int a1, int a2) { return Math.Min(a1, a2); }

        public bool Less(int a1, int a2) { return a1 < a2; }
        public bool Eq(int a1, int a2) { return a1 == a2; }
    }
}
