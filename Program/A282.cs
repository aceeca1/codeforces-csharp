using System;

class A282 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int x = 0;
        for (int i = 0; i < n; ++i)
            switch (Console.ReadLine()[1]) {
                case '+': ++x; break;
                case '-': --x; break;
            }
        Console.WriteLine(x);
    }
}
