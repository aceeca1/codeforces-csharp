﻿using System;
using System.Collections.Generic;
using System.Linq;

class A554 {
    static IEnumerable<string> AllSpecial(string s) {
        for (char i = 'a'; i <= 'z'; ++i) {
            var ii = new string(i, 1);
            for (int j = 0; j <= s.Length; ++j) yield return s.Insert(j, ii);
        }
    }

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(AllSpecial(s).Distinct().Count());
    }
}
