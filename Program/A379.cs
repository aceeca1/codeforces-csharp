﻿using System;

class A379 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        int ans = 0, used = 0;
        while (a != 0) {
            ans += a;
            used += a;
            a = used / b;
            used %= b;
        }
        Console.WriteLine(ans);
    }
}
