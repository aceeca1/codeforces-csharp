using System;
using System.Collections.Generic;
using System.Linq;

class A680 {
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new Dictionary<int, int>();
        foreach (var i in a) {
            int v;
            b.TryGetValue(i, out v);
            b[i] = v + 1;
        }
        int max = 0;
        foreach (var i in b)
            if (2 <= i.Value) 
                max = Math.Max(max, Math.Min(i.Value, 3) * i.Key);
        Console.WriteLine(a.Sum() - max);
    }
}
