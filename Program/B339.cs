﻿using System;

class B339 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int position = 1;
        long ans = 0L;
        foreach (var i in a) {
            ans += i - position;
            if (i < position) ans += n;
            position = i;
        }
        Console.WriteLine(ans);
    }
}
