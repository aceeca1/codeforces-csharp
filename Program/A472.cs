﻿using System;

class A472 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((n & 1) == 0 ? $"4 {n - 4}" : $"9 {n - 9}");
    }
}
