using System;

class B363 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int sum = 0, min = int.MaxValue, arg = -1;
        for (int i = 0; i < k; ++i) sum += a[i];
        for (int i = k; ; ++i) {
            if (sum < min) {
                min = sum;
                arg = i - k;
            }
            if (i == n) break;
            sum = sum + a[i] - a[i - k];
        }
        Console.WriteLine(arg + 1);
    }
}
