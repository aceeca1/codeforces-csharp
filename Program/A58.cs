using System;

class A58 {
    static bool HasSubseq(string s1, string s2) {
        var k = 0;
        foreach (var i in s1) {
            if (s2[k] == i) ++k;
            if (k == s2.Length) return true;
        }
        return false;
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(HasSubseq(s, "hello") ? "YES" : "NO");
    }
}
