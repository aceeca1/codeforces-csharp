using System;

class A746 {
    static int Min3(int a1, int a2, int a3) {
        return Math.Min(a1, Math.Min(a2, a3));
    }
    
    public static void Main() {
        var a = int.Parse(Console.ReadLine());
        var b = int.Parse(Console.ReadLine());
        var c = int.Parse(Console.ReadLine());
        Console.WriteLine(Min3(a, b >> 1, c >> 2) * 7);
    }
}
