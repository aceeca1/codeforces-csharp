using System;

class A620 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var x1 = int.Parse(line[0]);
        var y1 = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var x2 = int.Parse(line[0]);
        var y2 = int.Parse(line[1]);
        Console.WriteLine(Math.Max(Math.Abs(x2 - x1), Math.Abs(y2 - y1)));
    }
}
