using System;

class B545 {
    public static void Main() {
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        int hamming = 0;
        for (int i = 0; i < s1.Length; ++i)
            if (s1[i] != s2[i]) ++hamming;
        if ((hamming & 1) != 0) Console.WriteLine("impossible");
        else {
            hamming >>= 1;
            var s3 = s1.ToCharArray();
            for (int i = 0; i < s1.Length; ++i)
                if (s1[i] != s2[i]) {
                    if (hamming-- == 0) break;
                    s3[i] = s2[i];
                }
            Console.WriteLine(s3);
        }
    }
}
