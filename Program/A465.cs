using System;

class A465 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        int p = 0;
        while (p < n && s[p] == '1') ++p;
        Console.WriteLine(Math.Min(p + 1, n));
    }
}
