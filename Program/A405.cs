﻿using System;

class A405 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        Console.WriteLine(string.Join(" ", a));
    }
}
