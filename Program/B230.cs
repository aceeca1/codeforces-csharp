using System;
using System.Collections.Generic;
using System.Linq;

class B230 {
    static Prime prime;

    static bool IsTPrime(long n) {
        var k = (long)Math.Round(Math.Sqrt(n));
        return n == k * k && prime.Is((int)k);
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), long.Parse);
        prime = new Prime((int)Math.Round(Math.Sqrt(a.Max())));
        for (int i = 0; i < n; ++i)
            Console.WriteLine(IsTPrime(a[i]) ? "YES" : "NO");
    }

    // Snippet: Prime
    class Prime {
        int[] A;

        public Prime(int n) {
            A = new int[n + 1];
            int u = 0;
            for (int i = 2; i <= n; ++i) {
                int v = A[i];
                if (v == 0) u = A[u] = v = i;
                for (int w = 2; i * w <= n; w = A[w]) {
                    A[i * w] = w;
                    if (w >= v) break;
                }
            }
            A[u] = n + 1;
        }

        public bool Is(int n) { return n >= 2 && A[n] > n; }
        public int Next(int n) { return A[n]; }
        public int MinFactor(int n) { return Math.Min(n, A[n]); }

        public IEnumerable<int> From(int n) {
            while (n < A.Length) {
                yield return n;
                n = A[n];
            }
        }
    }
}
