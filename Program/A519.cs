using System;
using System.Linq;

class A519 {
    const string Piece  = "qrbnp";
    const string Weight = "95331";

    static int Count(string[] board, char c) {
        return board.Sum(k => k.Count(k1 => k1 == c));
    }

    public static void Main() {
        var board = new string[8];
        for (int i = 0; i < 8; ++i) board[i] = Console.ReadLine();
        int balance = 0;
        for (int i = 0; i < 5; ++i) {
            var w = Weight[i] - '0';
            var black = Count(board, Piece[i]) * w;
            var white = Count(board, char.ToUpper(Piece[i])) * w;
            balance = balance + (black - white);
        }
        Console.WriteLine(
            balance > 0 ? "Black" :
            balance < 0 ? "White" : "Draw"
        );
    }
}
