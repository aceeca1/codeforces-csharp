using System;
using System.Linq;

class A841 {
	public static void Main() {
		var line = Console.ReadLine().Split();
		var n = int.Parse(line[0]);
		var k = int.Parse(line[1]);
		var s = Console.ReadLine();
		var a = s.GroupBy(k1 => k1).Max(k1 => k1.Count());
		Console.WriteLine(k < a ? "NO" : "YES");
	}
}
