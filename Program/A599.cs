using System;
using System.Linq;

class A599 {
    public static void Main() {
        var d = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(new [] {
            d[0] + d[1] + d[2],
            d[0] + d[0] + d[2] + d[2],
            d[1] + d[1] + d[2] + d[2],
            d[0] + d[0] + d[1] + d[1]
        }.Min());
    }
}
