using System;

class A149 {
    public static void Main() {
        var k = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        int i = a.Length - 1;
        while (i >= 0 && k > 0) k -= a[i--];
        if (k > 0) Console.WriteLine(-1);
        else Console.WriteLine(a.Length - 1 - i);
    }
}
