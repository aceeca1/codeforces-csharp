using System;

class A4 {
    public static void Main() {
        var w = int.Parse(Console.ReadLine());
        Console.WriteLine(w >= 4 && (w & 1) == 0 ? "YES" : "NO");
    }
}
