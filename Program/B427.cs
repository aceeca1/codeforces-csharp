using System;

class B427 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int s = 0, answer = 0;
        for (int i = 0; i < n; ++i) {
            if (a[i] <= t) ++s; else s = 0;
            if (c <= s) ++answer;
        }
        Console.WriteLine(answer);
    }
}
