using System;

class A611 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        switch (line[2]) {
            case "week":
                var weekday = int.Parse(line[0]) % 7;
                var first = new DateTime(2016, 1, 1);
                var firstDay = (int)first.DayOfWeek;
                first = first.AddDays((weekday + 7 - firstDay) % 7);
                var last = new DateTime(2016, 12, 31);
                var lastDay = (int)last.DayOfWeek;
                last = last.AddDays(-((lastDay + 7 - weekday) % 7));
                Console.WriteLine((last - first).TotalDays / 7 + 1);
                break;
            case "month":
                var monthDay = int.Parse(line[0]);
                int ans = 0;
                for (int i = 1; i <= 12; ++i)
                    if (DateTime.DaysInMonth(2016, i) >= monthDay) ++ans;
                Console.WriteLine(ans);
                break;
        }
    }
}
