using System;
using System.Numerics;

class B507 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var r = double.Parse(line[0]);
        var x = double.Parse(line[1]);
        var y = double.Parse(line[2]);
        var x1 = double.Parse(line[3]);
        var y1 = double.Parse(line[4]);
        var p = new Complex(x, y);
        var p1 = new Complex(x1, y1);
        Console.WriteLine(Math.Ceiling(Complex.Abs(p - p1) / (r + r)));
    }
}
