using System;
using System.Linq;

class A570 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = new int[n];
        for (int i = 0; i < m; ++i) {
            var b = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            ++a[Array.IndexOf(b, b.Max())];
        }
        Console.WriteLine(Array.IndexOf(a, a.Max()) + 1);
    }
}
