﻿using System;
using System.Collections.Generic;
using System.Linq;

class A268 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<int, int[]>();
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            for (int j = 0; j < 2; ++j) {
                var k = int.Parse(line[j]);
                if (!a.ContainsKey(k)) a[k] = new int[2];
                ++a[k][j];
            }
        }
        Console.WriteLine(a.Sum(kv => kv.Value[0] * kv.Value[1]));
    }
}
