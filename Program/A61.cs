﻿using System;

class A61 {
    public static void Main() {
        var s1 = Console.ReadLine().ToCharArray();
        var s2 = Console.ReadLine();
        for (int i = 0; i < s1.Length; ++i)
            s1[i] = s1[i] == s2[i] ? '0' : '1';
        Console.WriteLine(new string(s1));
    }
}
