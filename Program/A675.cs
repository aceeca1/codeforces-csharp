using System;

class A675 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var d = b - a;
        bool ans = d == 0 || c != 0 && d % c == 0 && (d ^ c) >= 0;
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
