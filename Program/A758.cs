using System;
using System.Linq;

class A758 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(a.Max() * n - a.Sum());
    }
}
