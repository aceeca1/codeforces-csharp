using System;
using System.Linq;

class A621 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), long.Parse);
        var sum = a.Sum();
        if ((sum & 1) != 0) sum -= a.Where(k => (k & 1) != 0).Min();
        Console.WriteLine(sum);
    }
}
