using System;
using System.Linq;

class A467 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(Enumerable.Range(0, n).Count(k => {
            var line = Console.ReadLine().Split();
            var p = int.Parse(line[0]);
            var q = int.Parse(line[1]);
            return q - p >= 2;
        }));
    }
}
