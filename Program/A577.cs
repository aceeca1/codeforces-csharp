﻿using System;
using System.Linq;

class A577 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = int.Parse(line[1]);
        Console.WriteLine(Enumerable.Range(1, n).Count(k => 
            x / k <= n && x % k == 0
        ));
    }
}
