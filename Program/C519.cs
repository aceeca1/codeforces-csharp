using System;

class C519 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        if (n + n < m) m = n + n;
        if (m + m < n) n = m + m;
        Console.WriteLine((n + m) / 3);
    }
}
