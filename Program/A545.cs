using System;
using System.Collections.Generic;

class A545 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = new List<int>();
        for (int i = 0; i < n; ++i) {
            var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            if (!Array.Exists(a, k => k == 1 || k == 3)) ans.Add(i + 1);
        }
        Console.WriteLine(ans.Count);
        if (ans.Count != 0) Console.WriteLine(string.Join(" ", ans));
    }
}
