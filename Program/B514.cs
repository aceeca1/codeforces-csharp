using System;
using System.Collections.Generic;

class B514 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x0 = int.Parse(line[1]);
        var y0 = int.Parse(line[2]);
        var a = new HashSet<double>();
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var x = int.Parse(line[0]);
            var y = int.Parse(line[1]);
            var c = x == x0
                ? double.PositiveInfinity
                : (double)(y - y0) / (x - x0);
            a.Add(c);
        }
        Console.WriteLine(a.Count);
    }
}
