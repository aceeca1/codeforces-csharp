﻿using System;

class A510 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var s = new char[m];
        for (int i = 0; i < m; ++i) s[i] = '#';
        var s1 = new string(s);
        for (int i = 0; i < m; ++i) s[i] = '.';
        s[s.Length - 1] = '#';
        var s2 = new string(s);
        for (int i = 0; i < m; ++i) s[i] = '.';
        s[0] = '#';
        var s3 = new string(s);
        var ss = new string[] { s1, s2, s1, s3 };
        for (int i = 0; i < n; ++i) Console.WriteLine(ss[i & 3]);
    }
}
