using System;

class A864 {
    static bool Fair(int[] a) {
        Array.Sort(a);
        if (a[0] == a[a.Length - 1]) return false;
        var half = a.Length >> 1;
        for (int i = 0; i < half; ++i)
            if (a[i] != a[0]) return false;
        for (int i = half; i < a.Length; ++i)
            if (a[i] != a[a.Length - 1]) return false;
        return true;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        if (Fair(a)) {
            Console.WriteLine("YES");
            Console.WriteLine($"{a[0]} {a[a.Length - 1]}");
        } else Console.WriteLine("NO");
    }
}
