using System;

class A151 {
    static int Min3(int a1, int a2, int a3) {
        return Math.Min(a1, Math.Min(a2, a3));
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var l = int.Parse(line[2]);
        var c = int.Parse(line[3]);
        var d = int.Parse(line[4]);
        var p = int.Parse(line[5]);
        var nl = int.Parse(line[6]);
        var np = int.Parse(line[7]);
        Console.WriteLine(Min3(k * l / nl, c * d, p / np) / n);
    }
}
