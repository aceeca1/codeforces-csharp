using System;
using System.Linq;

class A131 {
    static string Correct(string s) {
        if (s.Skip(1).All(k => char.IsUpper(k))) {
            return string.Concat(s.Select(k => 
                char.IsUpper(k) ? char.ToLower(k) : char.ToUpper(k)
            ));
        } else return s;
    }
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Correct(s));
    }
}
