﻿using System;
using System.Collections.Generic;
using System.Linq;

class A500 {
    static IEnumerable<int> CanReach(int[] a) {
        for (int i = 0; ; i += a[i]) {
            yield return i;
            if (a.Length <= i) break;
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]) - 1;
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(CanReach(a).Any(k => k == t) ? "YES" : "NO");
    }
}
