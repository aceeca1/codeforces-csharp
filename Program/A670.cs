using System;

class A670 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = n / 7 * 2;
        var b = n % 7;
        var min = a + Math.Max(0, b - 5);
        var max = a + Math.Min(2, b);
        Console.WriteLine($"{min} {max}");
    }
}
