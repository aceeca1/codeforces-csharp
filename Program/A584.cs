using System;

class A584 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        if (t == 10) {
            if (n == 1) Console.WriteLine(-1);
            else Console.WriteLine("1" + new string('0', n - 1));
        } else Console.WriteLine(new string((char)('0' + t), n));
    }
}
