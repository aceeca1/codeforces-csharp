using System;

class A214 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        int ans = 0;
        for (int a = 0; a * a <= n; ++a) {
            int b = n - a * a;
            if (a + b * b == m) ++ans;
        }
        Console.WriteLine(ans);
    }
}
