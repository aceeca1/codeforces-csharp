using System;

class B831 {
    public static void Main() {
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        var translate = new char[256];
        for (int i = 0; i < 256; ++i) translate[i] = (char)i;
        for (int i = 0; i < 26; ++i) {
            translate[s1[i]] = s2[i];
            translate[s1[i] - 32] = (char)(s2[i] - 32);
        }
        var s = Console.ReadLine().ToCharArray();
        Console.WriteLine(Array.ConvertAll(s, k => translate[k]));
    }
}
