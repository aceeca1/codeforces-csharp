using System;

class C479 {
    class Exam {
        public int A, B;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var e = new Exam[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            e[i] = new Exam {
                A = int.Parse(line[0]),
                B = int.Parse(line[1])
            };
        }
        Array.Sort(e, (e1, e2) => {
            int c = e1.A.CompareTo(e2.A);
            return c != 0 ? c : e1.B.CompareTo(e2.B);
        });
        int current = 0;
        for (int i = 0; i < n; ++i)
            if (e[i].B < current) current = e[i].A;
            else current = e[i].B;
        Console.WriteLine(current);
    }
}
