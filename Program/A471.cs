using System;
using System.Collections.Generic;
using System.Linq;

class A471 {
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        var b = new List<int>(a.Where(k => k != a[2]));
        while (b.Count < 2) b.Add(a[2]);
        Console.WriteLine(
            2 < b.Count ? "Alien" :
            b[0] == b[1] ? "Elephant" : "Bear"
        );
    }
}
