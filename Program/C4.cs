using System;
using System.Collections.Generic;

class C4 {
    class Database {
        Dictionary<string, int> H = new Dictionary<string, int>();
        
        public string UniqueName(string s) {
            int n;
            if (H.TryGetValue(s, out n)) return s + (H[s] = n + 1);
            H[s] = 0;
            return "OK";
        }
    }

    public static void Main() {
        var db = new Database();
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            Console.WriteLine(db.UniqueName(s));
        }
    }
}
