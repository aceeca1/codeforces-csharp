using System;

class A384 {
    static void Write(int n, char c1, char c2) {
        var a = new char[n];
        for (int i = 0; i < n; ++i)
            a[i] = (i & 1) == 0 ? c1 : c2;
        Console.WriteLine(new string(a));
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((n * n + 1) >> 1);
        for (int i = 0; i < n; ++i)
            if ((i & 1) == 0) Write(n, 'C', '.');
            else Write(n, '.', 'C');
    }
}
