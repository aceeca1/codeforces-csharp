﻿using System;
using System.Linq;

class A474 {
    const string keyboard = "qwertyuiopasdfghjkl;zxcvbnm,./";

    public static void Main() {
        var m = new char[256];
        switch (Console.ReadLine()[0]) {
            case 'L':
                for (int i = 0; i < keyboard.Length - 1; ++i)
                    m[keyboard[i]] = keyboard[i + 1];
                break;
            case 'R':
                for (int i = 0; i < keyboard.Length - 1; ++i)
                    m[keyboard[i + 1]] = keyboard[i];
                break;
        }
        var ans = Console.ReadLine().Select(k => m[k]);
        Console.WriteLine(String.Concat(ans));
    }
}
