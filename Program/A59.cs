﻿using System;
using System.Linq;

class A59 {
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(s.Count(char.IsUpper) > s.Length >> 1
            ? s.ToUpper()
            : s.ToLower()
        );
    }
}
