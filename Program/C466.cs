using System;
using System.Linq;

class C466 {
    static long Ways(long[] a) {
        var sum = a.Sum();
        if (sum % 3 != 0) return 0L;
        var sum3 = sum / 3;
        var b1 = new int[a.Length];
        long pSum = 0L;
        for (int i = 0; i < a.Length; ++i) {
            pSum += a[i];
            b1[i] = pSum == sum3 ? 1 : 0;
        }
        for (int i = 1; i < a.Length; ++i) b1[i] += b1[i - 1];
        var b2 = new int[a.Length];
        long sSum = 0L;
        for (int i = a.Length - 1; i >= 0; --i) {
            sSum += a[i];
            b2[i] = sSum == sum3 ? 1 : 0;
        }
        long ans = 0L;
        for (int i = 2; i < a.Length; ++i)
            if (b2[i] != 0) ans += b1[i - 2];
        return ans;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), long.Parse);
        Console.WriteLine(Ways(a));
    }
}
