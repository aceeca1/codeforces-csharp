using System;
using System.Linq;

class A903 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        var b = new bool[a.Max() + 1];
        b[0] = true;
        foreach (var i in new int[]{3, 7})
            for (int j = i; j < b.Length; ++j)
                if (b[j - i]) b[j] = true;
        foreach (var i in a) Console.WriteLine(b[i] ? "YES" : "NO");
    }
}
