using System;
using System.Collections.Generic;

class A334 {
    static IEnumerable<int> GetLine(int n, int k) {
        int p = 0;
        for (int i = k; i < n; ++i) yield return (p++) * n + i + 1;
        for (int i = 0; i < k; ++i) yield return (p++) * n + i + 1;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) 
            Console.WriteLine(string.Join(" ", GetLine(n, i)));
    }
}
