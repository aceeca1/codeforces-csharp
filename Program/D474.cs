using System;
using System.Linq;

class D474 {
    const int M = 1000000007;

    public static void Main() {
        var line = Console.ReadLine().Split();
        var t = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a1 = new int[t];
        var a2 = new int[t];
        for (int i = 0; i < t; ++i) {
            line = Console.ReadLine().Split();
            a1[i] = int.Parse(line[0]);
            a2[i] = int.Parse(line[1]);
        }
        var m = a2.Max();
        var b = new int[m + 1];
        b[0] = 1;
        for (int i = 1; i <= m; ++i) {
            var t1 = 1 <= i ? b[i - 1] : 0;
            var t2 = k <= i ? b[i - k] : 0;
            b[i] = (t1 + t2) % M;
        }
        for (int i = 1; i <= m; ++i) b[i] = (b[i] + b[i - 1]) % M;
        for (int i = 0; i < t; ++i) {
            var t1 = b[a2[i]];
            var t2 = 1 <= a1[i] ? b[a1[i] - 1] : 0;
            Console.WriteLine((t1 + M - t2) % M);
        }
    }
}
