using System;

class A805 {
	public static void Main() {
		var line = Console.ReadLine().Split();
		var l = int.Parse(line[0]);
		var r = int.Parse(line[1]);
		if (l == r) Console.WriteLine(l);
		else Console.WriteLine(2);
	}
}
