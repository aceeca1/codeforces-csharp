using System;

class B313 {
    public static void Main() {
        var s = Console.ReadLine();
        var a = new int[s.Length];
        for (int i = 1; i < a.Length; ++i) 
            a[i] = s[i] == s[i - 1] ? a[i - 1] + 1 : a[i - 1];
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var l = int.Parse(line[0]) - 1;
            var r = int.Parse(line[1]) - 1;
            Console.WriteLine(a[r] - a[l]);
        }
    }
}
