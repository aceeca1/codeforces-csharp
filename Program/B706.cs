using System;
using System.Linq;

class B706 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[a.Max() + 1];
        foreach (var i in a) ++b[i];
        for (int i = 1; i < b.Length; ++i) b[i] += b[i - 1];
        var q = int.Parse(Console.ReadLine());
        for (int i = 0; i < q; ++i) {
            var c = int.Parse(Console.ReadLine());
            if (b.Length <= c) c = b.Length - 1;
            Console.WriteLine(b[c]);
        }
    }
}
