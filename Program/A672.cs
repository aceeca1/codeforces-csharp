using System;

class A672 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int a = 9, b = 1, c = 9;
        while (n > c) {
            n -= c;
            a *= 10;
            ++b;
            c = a * b;
        }
        int num = a / 9 + (n - 1) / b;
        Console.WriteLine(num.ToString()[(n - 1) % b]);
    }
}
