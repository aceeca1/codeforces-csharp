﻿using System;
using System.Linq;

class A768 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var min = a.Min();
        var max = a.Max();
        Console.WriteLine(a.Count(k => min < k && k < max));
    }
}
