using System;

class A462 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        bool ans = true;
        for (int i1 = 0; i1 < n; ++i1)
            for (int i2 = 0; i2 < n; ++i2) {
                var a1 = 0 < i1 && a[i1 - 1][i2] == 'o' ? 1 : 0;
                var a2 = 0 < i2 && a[i1][i2 - 1] == 'o' ? 1 : 0;
                var a3 = i1 < n - 1 && a[i1 + 1][i2] == 'o' ? 1 : 0;
                var a4 = i2 < n - 1 && a[i1][i2 + 1] == 'o' ? 1 : 0;
                if (((a1 + a2 + a3 + a4) & 1) != 0) ans = false;
            }
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
