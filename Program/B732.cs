using System;
using System.Linq;

class B732 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var sum = a.Sum();
        for (int i = 1; i < n; ++i)
            if (a[i - 1] + a[i] < k) a[i] = k - a[i - 1];
        Console.WriteLine(a.Sum() - sum);
        Console.WriteLine(string.Join(" ", a));
    }
}
