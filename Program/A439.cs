using System;
using System.Linq;

class A439 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var minTime = a.Sum() + 10 * (n - 1);
        Console.WriteLine(d < minTime
            ? -1
            : (d - minTime) / 5 + ((n - 1) << 1)
        );
    }
}
