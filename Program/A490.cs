﻿using System;
using System.Collections.Generic;
using System.Linq;

class A490 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new List<int>[3];
        for (int i = 0; i < 3; ++i) b[i] = new List<int>();
        for (int i = 0; i < n; ++i) b[a[i] - 1].Add(i + 1);
        var ans = b.Min(k => k.Count);
        Console.WriteLine(ans);
        if (ans != 0)
            for (int i = 0; i < ans; ++i)
                Console.WriteLine(string.Join(" ", b.Select(k => k[i])));
    }
}
