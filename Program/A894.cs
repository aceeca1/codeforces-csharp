using System;

class A894 {
    public static void Main() {
        var s = Console.ReadLine();
        int q0 = 0, q1 = 0, q2 = 0;
        foreach (var i in s)
            if (i == 'Q') { ++q0; q2 += q1; }
            else if (i == 'A') { q1 += q0; }
        Console.WriteLine(q2);
    }
}
