﻿using System;

class A703 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int win0 = 0, win1 = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a0 = int.Parse(line[0]);
            var a1 = int.Parse(line[1]);
            if (a1 < a0) ++win0;
            if (a0 < a1) ++win1;
        }
        Console.WriteLine(
            win1 < win0 ? "Mishka" :
            win0 < win1 ? "Chris" : "Friendship is magic!^^"
        );
    }
}
