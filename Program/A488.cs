using System;

class A488 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var m = n + 1;
        while (m.ToString().IndexOf('8') == -1) ++m;
        Console.WriteLine(m - n);
    }
}
