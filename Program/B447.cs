using System;
using System.Linq;

class B447 {
    public static void Main() {
        var s = Console.ReadLine();
        var k = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var answer = 0;
        for (int i = 0; i < s.Length; ++i) answer += (i + 1) * a[s[i] - 'a'];
        var aMax = a.Max();
        for (int i = s.Length; i < s.Length + k; ++i) answer += (i + 1) * aMax;
        Console.WriteLine(answer);
    }
}
