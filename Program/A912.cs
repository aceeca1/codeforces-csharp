using System;

class A912 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = long.Parse(line[0]);
        var b = long.Parse(line[1]);
        line = Console.ReadLine().Split();
        var x = long.Parse(line[0]);
        var y = long.Parse(line[1]);
        var z = long.Parse(line[2]);
        var aNeed = x + x + y;
        var bNeed = y + z + z + z;
        Console.WriteLine(Math.Max(aNeed - a, 0L) + Math.Max(bNeed - b, 0L));
    }
}
