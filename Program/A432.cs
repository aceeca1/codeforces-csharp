using System;
using System.Linq;

class A432 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var y = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(y.Count(i => k <= 5 - i) / 3);
    }
}
