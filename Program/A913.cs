using System;

class A913 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var m = int.Parse(Console.ReadLine());
        if (30 < n) n = 30;
        Console.WriteLine(m & ((1 << n) - 1));
    }
}
