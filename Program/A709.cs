using System;

class A709 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var d = int.Parse(line[2]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int answer = 0;
        int trash = 0;
        for (int i = 0; i < n; ++i)
            if (a[i] <= b) {
                trash += a[i];
                if (d < trash) { ++answer; trash = 0; }
            }
        Console.WriteLine(answer);
    }
}
