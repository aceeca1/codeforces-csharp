using System;

class A514 {
    public static void Main() {
        var s = Console.ReadLine().ToCharArray();
        for (int i = s[0] == '9' ? 1 : 0; i < s.Length; ++i)
            s[i] = (char)Math.Min(s[i], '0' + ('9' - s[i]));
        Console.WriteLine(new string(s));
    }
}
