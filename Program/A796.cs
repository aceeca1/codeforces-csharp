using System;

class A796 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]) - 1;
        var k = int.Parse(line[2]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int min = int.MaxValue;
        for (int i = 0; i < n; ++i)
            if (0 != a[i] && a[i] <= k)
                min = Math.Min(min, Math.Abs(i - m));
        Console.WriteLine(min * 10);
    }
}
