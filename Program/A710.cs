using System;

class A710 {
    public static void Main() {
        var s = Console.ReadLine();
        var side1 = s[0] == 'a' || s[0] == 'h' ? 1 : 0;
        var side2 = s[1] == '1' || s[1] == '8' ? 1 : 0;
        switch (side1 + side2) {
            case 0: Console.WriteLine(8); break;
            case 1: Console.WriteLine(5); break;
            case 2: Console.WriteLine(3); break;
        }
    }
}
