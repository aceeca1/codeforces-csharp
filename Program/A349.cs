using System;

class A349 {
    static bool Can(int[] a) {
        int c25 = 0, c50 = 0;
        foreach (var i in a)
            switch (i) {
                case 25: ++c25; break;
                case 50: 
                    if (c25 < 1) return false;
                    --c25; ++c50; break;
                case 100:
                    if (c50 < 1) {
                        if (c25 < 3) return false;
                        c25 -= 3; break;
                    } else {
                        if (c25 < 1) return false;
                        --c25; --c50; break;
                    }
            }
        return true;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(Can(a) ? "YES" : "NO");
    }
}
