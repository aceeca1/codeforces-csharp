﻿using System;
using System.Linq;

class A69 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = new int[3];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var line1 = Array.ConvertAll(line, int.Parse);
            for (int j = 0; j < 3; ++j) ans[j] += line1[j];
        }
        Console.WriteLine(ans.All(k => k == 0) ? "YES" : "NO");
    }
}
