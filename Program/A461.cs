using System;

class A461 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        long ans = -a[a.Length - 1];
        for (int i = 0; i < n; ++i) ans += (long)(i + 2) * a[i];
        Console.WriteLine(ans);
    }
}
