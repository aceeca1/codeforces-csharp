using System;

class B546 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n + n + 1];
        foreach (var i in a) ++b[i];
        int ans = 0;
        for (int i = 0; i < b.Length - 1; ++i) {
            var v = Math.Max(b[i] - 1, 0);
            b[i + 1] += v;
            ans += v;
        }
        Console.WriteLine(ans);
    }
}
