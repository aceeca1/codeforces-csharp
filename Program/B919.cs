using System;
using System.Collections.Generic;
using System.Linq;

class B919 {
    static IEnumerable<int> DigitSum10(
        int size, int prefix, int prefixSize, int prefixSum) {
        if (prefixSize == size) {
            if (prefixSum == 10) yield return prefix;
        } else if (prefixSum <= 10) {
            var lower = prefix == 0 ? 1 : 0;
            for (int i = lower; i < 10; ++i)
                foreach (var j in DigitSum10(
                    size, prefix * 10 + i, prefixSize + 1, prefixSum + i))
                    yield return j;
        }
    }
    
    static IEnumerable<int> DigitSum10() {
        for (int i = 1;; ++i)
            foreach (var j in DigitSum10(i, 0, 0, 0)) yield return j;
    }
    
    public static void Main() {
        var k = int.Parse(Console.ReadLine());
        Console.WriteLine(DigitSum10().ElementAt(k - 1));
    }
}
