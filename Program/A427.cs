using System;

class A427 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int c = 0, ans = 0;
        for (int i = 0; i < n; ++i) {
            c += a[i];
            if (c < 0) { ans -= c; c = 0; }
        }
        Console.WriteLine(ans);
    }
}
