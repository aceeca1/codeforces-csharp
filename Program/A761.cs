using System;

class A761 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        Console.WriteLine(0 != a + b && Math.Abs(a - b) <= 1 ? "YES" : "NO");
    }
}
