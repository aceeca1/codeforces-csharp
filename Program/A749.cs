using System;
using System.Collections.Generic;

class A749 {
    static IEnumerable<int> Split(int n) {
        while (n > 3) {
            yield return 2;
            n -= 2;
        }
        yield return n;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(n >> 1);
        Console.WriteLine(string.Join(" ", Split(n)));
    }
}
