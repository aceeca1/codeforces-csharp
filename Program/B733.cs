using System;
using System.Linq;

class B733 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            a[i] = int.Parse(line[0]) - int.Parse(line[1]);
        }
        var sum = a.Sum();
        int max = Math.Abs(sum), arg = 0;
        for (int i = 0; i < n; ++i) {
            var v = Math.Abs(sum - (a[i] << 1));
            if (v > max) { max = v; arg = i + 1; }
        }
        Console.WriteLine(arg);
    }
}
