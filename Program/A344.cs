﻿using System;
using System.Collections.Generic;
using System.Linq;

class A344 {
    class PartitionBy : APartitionBy<string, int> {
        class Combiner : ICombiner {
            public void Add(string data) { }
            public int Result() => 0;
        }

        protected override IEnumerable<string> GetInput() => Input;
        protected override ICombiner MakeCombiner() => new Combiner();
        protected override bool IsEquivalent(string a1, string a2) => 
            a1 == a2;

        public IEnumerable<string> Input;
        public PartitionBy(IEnumerable<string> input) { Input = input; }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        Console.WriteLine(new PartitionBy(a).Result().Count());
    }

    // Snippet: PartitionBy
    abstract class APartitionBy<T, U> {
        public interface ICombiner {
            void Add(T data);
            U Result();
        }

        protected abstract IEnumerable<T> GetInput();
        protected abstract bool IsEquivalent(T a1, T a2);
        protected abstract ICombiner MakeCombiner();

        public IEnumerable<U> Result() {
            var data = default(T);
            var combiner = default(ICombiner);
            foreach (var i in GetInput())
                if (combiner == null) {
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                } else if (IsEquivalent(data, i)) {
                    data = i;
                    combiner.Add(i);
                } else {
                    yield return combiner.Result();
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                }
            if (combiner != null) yield return combiner.Result();
        }
    }
}