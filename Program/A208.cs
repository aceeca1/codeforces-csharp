﻿using System;
using System.Text.RegularExpressions;

class A208 {
    static Regex wub = new Regex("(WUB)+");

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(wub.Replace(s, " ").Trim());
    }
}
