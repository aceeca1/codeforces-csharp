using System;
using System.Linq;

class A915 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(k / a.Where(k1 => k % k1 == 0).Max());
    }
}
