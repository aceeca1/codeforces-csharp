using System;
using System.Text.RegularExpressions;

class A877 {
    static Regex Friends = new Regex("Danil|Olya|Slava|Ann|Nikita");

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Friends.Matches(s).Count == 1 ? "YES" : "NO");
    }
}
