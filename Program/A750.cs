using System;

class A750 {
    static int maxProblem(int n, int k) {
        var timeLeft = 240 - k;
        for (int i = 1; i <= n; ++i) {
            var need = 5 * i;
            if (timeLeft < need) return i - 1;
            timeLeft -= need;
        }
        return n;
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        Console.WriteLine(maxProblem(n, k));
    }
}
