using System;

class A548 {
    static bool IsPalindrome(string s) {
        var a = s.ToCharArray();
        Array.Reverse(a);
        return new string(a) == s;
    }

    static bool IsKPalindrome(string s, int k) {
        if (s.Length % k != 0) return false;
        int n = s.Length / k;
        for (int i = 0; i < k; ++i)
            if (!IsPalindrome(s.Substring(i * n, n))) return false;
        return true;
    }

    public static void Main() {
        var s = Console.ReadLine();
        var k = int.Parse(Console.ReadLine());
        Console.WriteLine(IsKPalindrome(s, k) ? "YES" : "NO");
    }
}
