﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

class B492 {
    static IEnumerable<double> NeedCover(int[] a, int l) {
        yield return a[0];
        for (int i = 0; i < a.Length - 1; ++i)
            yield return 0.5 * (a[i + 1] - a[i]);
        yield return l - a[a.Length - 1];
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var l = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        var ans = NeedCover(a, l).Max();
        Console.WriteLine(ans.ToString(CultureInfo.InvariantCulture));
    }
}
