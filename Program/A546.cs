using System;

class A546 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        var w = int.Parse(line[2]);
        var need = (w * (w + 1) >> 1) * k;
        Console.WriteLine(Math.Max(0, need - n));
    }
}
