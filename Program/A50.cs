using System;

class A50 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var m = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        Console.WriteLine(m * n >> 1);
    }
}
