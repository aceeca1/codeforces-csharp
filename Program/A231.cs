using System;
using System.Linq;

class A231 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(Enumerable.Range(0, n).Count(k =>
            Console.ReadLine().Split().Sum(int.Parse) >= 2
        ));
    }
}
