using System;
using System.Linq;

class A688 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = int.Parse(line[1]);
        int ans = 0, c = 0;
        for (int i = 0; i < d; ++i)
            if (Console.ReadLine().All(k => k == '1')) c = 0;
            else if (++c > ans) ans = c; 
        Console.WriteLine(ans);
    }
}
