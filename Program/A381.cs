using System;

class A381 {
	public static void Main() {
		var n = int.Parse(Console.ReadLine());
		var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
		int k1 = 0, k2 = a.Length - 1;
		var ans = new int[2];
		int curr = 0;
		while (k1 <= k2) {
			if (a[k1] < a[k2]) ans[curr] += a[k2--];
			else ans[curr] += a[k1++];
			curr = 1 - curr;
		}
		Console.WriteLine($"{ans[0]} {ans[1]}");
	}
}
