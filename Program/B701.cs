using System;
using System.Collections.Generic;

class B701 {
    static IEnumerable<long> ReadSolve(int n, int m) {
        var a1 = new HashSet<int>();
        var a2 = new HashSet<int>();
        for (int i = 0; i < m; ++i) {
            var line = Console.ReadLine().Split();
            a1.Add(int.Parse(line[0]));
            a2.Add(int.Parse(line[1]));
            yield return (long)(n - a1.Count) * (n - a2.Count);
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(string.Join(" ", ReadSolve(n, m)));
    }
}
