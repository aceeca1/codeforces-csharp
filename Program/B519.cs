﻿using System;
using System.Linq;

class B519 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new long[3];
        for (int i = 0; i < 3; ++i)
            a[i] = Console.ReadLine().Split().Sum(k => long.Parse(k));
        Console.WriteLine(a[0] - a[1]);
        Console.WriteLine(a[1] - a[2]);
    }
}
