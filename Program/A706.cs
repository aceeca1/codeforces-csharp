using System;
using System.Globalization;
using System.Numerics;

class A706 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var ax = double.Parse(line[0]);
        var ay = double.Parse(line[1]);
        var a = new Complex(ax, ay);
        var n = int.Parse(Console.ReadLine());
        double ans = double.PositiveInfinity;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var x = double.Parse(line[0]);
            var y = double.Parse(line[1]);
            var v = double.Parse(line[2]);
            var b = new Complex(x, y);
            var time = Complex.Abs(b - a) / v;
            if (time < ans) ans = time;
        }
        Console.WriteLine(ans.ToString(CultureInfo.InvariantCulture));
    }
}
