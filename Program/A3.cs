﻿using System;
using System.Linq;

class A3 {
    public static void Main() {
        var s1 = Console.ReadLine().ToCharArray();
        var s2 = Console.ReadLine().ToCharArray();
        var steps = Enumerable.Range(0, 2).Max(k => Math.Abs(s1[k] - s2[k]));
        Console.WriteLine(steps);
        while (true) {
            string s = "";
            if (s1[0] < s2[0]) { s += "R"; ++s1[0]; }
            if (s2[0] < s1[0]) { s += "L"; --s1[0]; }
            if (s1[1] < s2[1]) { s += "U"; ++s1[1]; }
            if (s2[1] < s1[1]) { s += "D"; --s1[1]; }
            if (s.Length == 0) break;
            Console.WriteLine(s);
        }
    }
}
