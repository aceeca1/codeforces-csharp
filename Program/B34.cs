﻿using System;

class B34 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        int ans = 0;
        for (int i = 0; i < m; ++i)
            if (a[i] < 0) ans -= a[i];
            else break;
        Console.WriteLine(ans);
    }
}
