using System;
using System.Linq;

class A262 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = Console.ReadLine().Split();
        Console.WriteLine(
            a.Count(s => s.Count(c => c == '4') + s.Count(c => c == '7') <= k)
        );
    }
}
