using System;
using System.Linq;

class A236 {
    public static void Main() {
        var female = (Console.ReadLine().Distinct().Count() & 1) == 0;
        Console.WriteLine(female ? "CHAT WITH HER!" : "IGNORE HIM!");
    }
}
