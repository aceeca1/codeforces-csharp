﻿using System;
using System.Linq;

class A466 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = int.Parse(line[2]);
        var b = int.Parse(line[3]);
        Console.WriteLine(new int[] {
            (n + m - 1) / m * b,
            n / m * b + n % m * a,
            n * a
        }.Min());
    }
}
