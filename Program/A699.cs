using System;

class A699 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var answer = int.MaxValue;
        for (int i = 0; i < s.Length - 1; ++i)
            if (s[i] == 'R' && s[i + 1] == 'L') {
                var v = (a[i + 1] - a[i]) >> 1;
                if (v < answer) answer = v;
            }
        if (answer == int.MaxValue) Console.WriteLine(-1);
        else Console.WriteLine(answer);
    }
}
