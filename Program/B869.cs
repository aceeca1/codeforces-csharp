using System;

class B869 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = long.Parse(line[0]);
        var b = long.Parse(line[1]);
        if (10L <= b - a) Console.WriteLine(0);
        else {
            long answer = 1;
            for (long i = a + 1; i <= b; ++i)
                answer = answer * (i % 10) % 10;
            Console.WriteLine(answer);
        }
    }
}
