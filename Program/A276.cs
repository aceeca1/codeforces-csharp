using System;

class A276 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        int ans = int.MinValue;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var f = int.Parse(line[0]);
            var t = int.Parse(line[1]);
            var v = f - Math.Max(0, t - k);
            if (v > ans) ans = v;
        }
        Console.WriteLine(ans);
    }
}
