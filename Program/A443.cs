﻿using System;
using System.Linq;

class A443 {
    public static void Main() {
        Console.WriteLine(Console.ReadLine().Split(
            new string[] { "{", ", ", "}" },
            StringSplitOptions.RemoveEmptyEntries
        ).Distinct().Count());
    }
}
