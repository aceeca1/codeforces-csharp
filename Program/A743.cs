using System;

class A743 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]) - 1;
        var b = int.Parse(line[2]) - 1;
        var s = Console.ReadLine();
        Console.WriteLine(s[a] == s[b] ? 0 : 1);
    }
}
