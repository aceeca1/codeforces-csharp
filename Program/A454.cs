using System;

class A454 {
    static void Write(int star, int d) {
        var s1 = new string('*', star);
        var s2 = new string('D', d);
        Console.WriteLine(s1 + s2 + s1);
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine()) >> 1;
        for (int i = 0; i <= n; ++i) Write(n - i, i + i + 1);
        for (int i = n - 1; i >= 0; --i) Write(n - i, i + i + 1);
    }
}
