using System;
using System.Linq;

class B672 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        if (26 < n) Console.WriteLine(-1);
        else {
            var a = new bool[26];
            foreach (var i in s) a[i - 'a'] = true;
            Console.WriteLine(n - a.Count(k => k));
        }
    }
}
