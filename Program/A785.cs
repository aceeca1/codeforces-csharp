using System;
using System.Collections.Generic;

class A785 {
    static Dictionary<string, int> Face = new Dictionary<string, int> {
        {"Tetrahedron", 4},
        {"Cube", 6},
        {"Octahedron", 8},
        {"Dodecahedron", 12},
        {"Icosahedron", 20}
    };
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int ans = 0;
        for (int i = 0; i < n; ++i)
            ans += Face[Console.ReadLine()];
        Console.WriteLine(ans);
    }
}
