﻿using System;
using System.Linq;

class A148 {
    public static void Main() {
        var a = new int[4];
        for (int i = 0; i < 4; ++i) a[i] = int.Parse(Console.ReadLine());
        var d = int.Parse(Console.ReadLine());
        Console.WriteLine(Enumerable.Range(1, d).Count(i =>
            Array.Exists(a, k => i % k == 0)
        ));
    }
}
