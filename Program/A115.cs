using System;
using System.Linq;

class A115 {
    static int Depth(int[] a, int[] b, int no) {
        if (no == -2) return 0;
        if (b[no] != -1) return b[no];
        return b[no] = Depth(a, b, a[no]) + 1;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        var b = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = int.Parse(Console.ReadLine()) - 1;
            b[i] = -1;
        }
        for (int i = 0; i < n; ++i) Depth(a, b, i);
        Console.WriteLine(b.Max());
    }
}
