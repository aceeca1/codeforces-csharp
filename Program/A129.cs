using System;
using System.Linq;

class A129 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var s = a.Sum();
        Console.WriteLine(a.Count(k => ((s - k) & 1) == 0));
    }
}
