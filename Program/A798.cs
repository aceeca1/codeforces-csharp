using System;
using System.Linq;

class A798 {
    public static void Main() {
        var s = Console.ReadLine();
        var a = Enumerable.Range(0, s.Length).Count(
            k => s[k] != s[s.Length - 1 - k]
        );
        var ans = a == 2 || a == 0 && (s.Length & 1) != 0;
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
