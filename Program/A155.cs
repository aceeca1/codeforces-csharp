using System;

class A155 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int min = a[0], max = a[0], ans = 0;
        for (int i = 1; i < a.Length; ++i) {
            if (max < a[i])       { max = a[i]; ++ans; }
            else if (a[i] < min)  { min = a[i]; ++ans; }
        }
        Console.WriteLine(ans);
    }
}
