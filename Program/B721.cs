using System;

class B721 {
    const int M = 100;

    static int f(int n, int k) {
        return n + n / k * 5 + 1;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = new int[M + 1];
        for (int i = 0; i < n; ++i) ++a[Console.ReadLine().Length];
        for (int i = 0; i < M; ++i) a[i + 1] += a[i];
        var t = Console.ReadLine().Length;
        Console.WriteLine($"{f(a[t - 1], k)} {f(a[t] - 1, k)}");
    }
}
