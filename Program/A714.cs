using System;

class A714 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var l1 = long.Parse(line[0]);
        var r1 = long.Parse(line[1]);
        var l2 = long.Parse(line[2]);
        var r2 = long.Parse(line[3]);
        var k = long.Parse(line[4]);
        var l = Math.Max(l1, l2);
        var r = Math.Min(r1, r2);
        var ans = Math.Max(0L, r - l + 1L);
        if (l <= k && k <= r) --ans;
        Console.WriteLine(ans);
    }
}
