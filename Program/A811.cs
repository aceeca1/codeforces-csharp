using System;

class A811 {
    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int step = 1;
        int d = 0;
        while (true) {
            a[d] -= step;
            if (a[d] < 0) break;
            ++step;
            d = 1 - d;
        }
        Console.WriteLine(new string[] { "Vladik", "Valera" } [d]);
    }
}
