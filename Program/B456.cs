using System;

class B456 {
    public static void Main() {
        var s = Console.ReadLine();
        var k = Math.Max(0, s.Length - 2);
        var a = s.Substring(k, s.Length - k);
        switch (int.Parse(a) & 3) {
            case 0: Console.WriteLine(4); break;
            case 1: Console.WriteLine(0); break;
            case 2: Console.WriteLine(0); break;
            case 3: Console.WriteLine(0); break;
        }
    }
}
