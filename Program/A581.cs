using System;

class A581 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var ans1 = Math.Min(a, b);
        var ans2 = ((a + b) >> 1) - ans1;
        Console.WriteLine($"{ans1} {ans2}");
    }
}
