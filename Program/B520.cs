using System;

class B520 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        int ans = 0;
        while (m != n) {
            if (m < n || (m & 1) != 0) ++m;
            else m >>= 1;
            ++ans;
        }
        Console.WriteLine(ans);
    }
}
