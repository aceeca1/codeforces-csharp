﻿using System;
using System.Linq;

class A734 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var a = s.Count(k => k == 'A');
        var d = s.Length - a;
        Console.WriteLine(a < d ? "Danik" : d < a ? "Anton" : "Friendship");
    }
}
