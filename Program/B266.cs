﻿using System;

class B266 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        var s = Console.ReadLine().ToCharArray();
        for (int i = 0; i < t; ++i)
            for (int j = 0; j < n - 1; ++j)
                if (s[j] < s[j + 1]) {
                    var sj = s[j];
                    s[j] = s[j + 1];
                    s[j + 1] = sj;
                    ++j;
                }
        Console.WriteLine(new string(s));
    }
}
