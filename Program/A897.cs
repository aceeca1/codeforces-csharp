using System;

class A897 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var s = Console.ReadLine().ToCharArray();
        for (int i = 0; i < m; ++i) {
            line = Console.ReadLine().Split();
            var l = int.Parse(line[0]) - 1;
            var r = int.Parse(line[1]) - 1;
            var c1 = line[2][0];
            var c2 = line[3][0];
            for (int j = l; j <= r; ++j)
                if (s[j] == c1) s[j] = c2;
        }
        Console.WriteLine(s);
    }
}
