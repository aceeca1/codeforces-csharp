﻿using System;

class A682 {
    static long F(int n, int k) {
        k = (k + 4) % 5 + 1;
        return (n + 5 - k) / 5;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        long answer = 0;
        for (int i = 0; i < 5; ++i) answer += F(n, i) * F(m, (5 - i) % 5);
        Console.WriteLine(answer);
    }
}
