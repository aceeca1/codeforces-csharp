using System;
using System.Collections.Generic;
using System.Linq;

class B454 {
    static IEnumerable<int> Shift(int[] a, int start) {
        for (int i = start; i < a.Length; ++i) yield return a[i];
        for (int i = 0; i < start; ++i) yield return a[i];
    }
    
    static bool Sorted(IEnumerable<int> a) {
        int last = int.MinValue;
        foreach (var i in a) {
            if (i < last) return false;
            last = i;
        }
        return true;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var aMin = a.Min();
        int start = -1;
        for (int i = 0; i < a.Length; ++i)
            if (a[i] == aMin && (i == 0 || a[i - 1] != aMin)) start = i;
        if (Sorted(Shift(a, start))) 
            Console.WriteLine(start == 0 ? 0 : n - start);
        else
            Console.WriteLine(-1);
    }
}
