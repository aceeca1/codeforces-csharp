using System;

class A854 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var m = n >> 1;
        switch (n & 3) {
            case 0: Console.WriteLine($"{m - 1} {m + 1}"); break;
            case 1: case 3: Console.WriteLine($"{m} {m + 1}"); break;
            case 2: Console.WriteLine($"{m - 2} {m + 2}"); break;
        }
    }
}
