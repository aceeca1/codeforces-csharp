using System;

class A835 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var s = int.Parse(line[0]);
        var v1 = int.Parse(line[1]);
        var v2 = int.Parse(line[2]);
        var t1 = int.Parse(line[3]);
        var t2 = int.Parse(line[4]);
        var total1 = t1 + v1 * s + t1;
        var total2 = t2 + v2 * s + t2;
        Console.WriteLine(
            total1 == total2 ? "Friendship" :
            total1 < total2 ? "First" : "Second"
        );
    }
}
