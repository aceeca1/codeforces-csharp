using System;

class A525 {
    public static void Main() {
        Console.ReadLine();
        var key = new int[26];
        int answer = 0;
        foreach (var i in Console.ReadLine())
            if (char.IsLower(i)) ++key[i - 'a'];
            else if (key[i - 'A'] != 0) --key[i - 'A'];
            else ++answer;
        Console.WriteLine(answer);
    }
}
