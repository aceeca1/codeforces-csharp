using System;

class A697 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var t = int.Parse(line[0]);
        var s = int.Parse(line[1]);
        var x = int.Parse(line[2]);
        bool bark = false;
        switch ((x - t) % s) {
            case 0: case 1:
                if (t <= x && x != t + 1) bark = true; break;
        }
        Console.WriteLine(bark ? "YES" : "NO");
    }
}
