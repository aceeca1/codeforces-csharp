using System;
using System.Linq;

class A939 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var answer = Enumerable.Range(0, n).Any(k => {
            return a[a[a[k] - 1] - 1] - 1 == k; 
        });
        Console.WriteLine(answer ? "YES" : "NO");
    }
}
