using System;
using System.Linq;

class B467 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var k = int.Parse(line[2]);
        var a = new int[m];
        for (int i = 0; i < m; ++i) a[i] = int.Parse(Console.ReadLine());
        var am = int.Parse(Console.ReadLine());
        Console.WriteLine(a.Count(k1 => BitCount.Int(k1 ^ am) <= k));
    }

    // Snippet: BitCount
    class BitCount {
        public static int Int(int n) {
            n = (n & 0x55555555) + ((n >> 1) & 0x55555555);
            n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
            n = (n & 0x0f0f0f0f) + ((n >> 4) & 0x0f0f0f0f);
            n = (n & 0x00ff00ff) + ((n >> 8) & 0x00ff00ff);
            return (n & 0xffff) + (n >> 16);
        }
    }
}
