using System;

class A513 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n1 = int.Parse(line[0]);
        var n2 = int.Parse(line[1]);
        Console.WriteLine(n1 > n2 ? "First" : "Second");
    }
}
