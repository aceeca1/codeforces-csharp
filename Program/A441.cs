using System;
using System.Collections.Generic;
using System.Linq;

class A441 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var v = int.Parse(line[1]);
        var ans = new List<int>();
        for (int i = 0; i < n; ++i) {
            var a = Console.ReadLine().Split().Select(int.Parse);
            if (a.Skip(1).Min() < v) ans.Add(i + 1);
        }
        Console.WriteLine(ans.Count);
        Console.WriteLine(string.Join(" ", ans));
    }
}
