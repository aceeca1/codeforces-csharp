using System;
using System.Linq;

class A478 {
    public static void Main() {
        var sum = Console.ReadLine().Split().Sum(int.Parse);
        Console.WriteLine(sum != 0 && sum % 5 == 0 ? sum / 5 : -1);
    }
}
