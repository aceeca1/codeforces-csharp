﻿using System;

class A483 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var l = long.Parse(line[0]);
        var r = long.Parse(line[1]);
        switch (r - l) {
            case 0L: case 1L: l = -1; break;
            case 2L: if ((l & 1) != 0) l = -1; break;
            default: if ((l & 1) != 0) ++l; break;
        }
        if (l == -1L) Console.WriteLine(-1);
        else Console.WriteLine($"{l} {l + 1} {l + 2}");
    }
}
