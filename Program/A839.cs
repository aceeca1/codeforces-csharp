using System;

class A839 {
    static int Solve() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int save = 0;
        for (int i = 0; i < n; ++i) {
            save += a[i];
            var give = Math.Min(8, save);
            save -= give;
            k -= give;
            if (k <= 0) return i + 1;
        }
        return -1;
    }
    
    public static void Main() {
        Console.WriteLine(Solve());
    }
}
