using System;

class B935 {
    public static void Main() {
        Console.ReadLine();
        int answer = 0, kingdom = 0, x = 0, y = 0;
        foreach (var i in Console.ReadLine())
            switch (i) {
                case 'R':
                    if (y < ++x) {
                        if (kingdom == -1) ++answer;
                        kingdom = 1;
                    } break;
                case 'U':
                    if (x < ++y) {
                        if (kingdom == 1) ++answer;
                        kingdom = -1;
                    } break;
            }
        Console.WriteLine(answer);
    }
}
