using System;

class B474 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var m = int.Parse(Console.ReadLine());
        var b = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 1; i < n; ++i) a[i] += a[i - 1];
        for (int i = 0; i < m; ++i)
            Console.WriteLine(BSearch.Int(1, n, k => b[i] <= a[k - 1]));
    }

    // Snippet: BSearch
    class BSearch {
        public static int Int(int s, int t, Func<int, bool> f) {
            for (;;) {
                if (s == t) return s;
                int m = s + ((t - s) >> 1);
                if (f(m)) t = m; else s = m + 1;
            }
        }
    }
}
