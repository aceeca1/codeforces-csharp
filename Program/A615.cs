using System;
using System.Linq;

class A615 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = new bool[m];
        for (int i = 0; i < n; ++i)
            foreach (var j in Console.ReadLine().Split().Skip(1))
                a[int.Parse(j) - 1] = true;
        Console.WriteLine(a.All(k => k) ? "YES" : "NO");
    }
}
