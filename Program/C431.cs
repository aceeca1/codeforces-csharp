using System;

class C431 {
    const int M = 1000000007;

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var d = int.Parse(line[2]);
        var a = new long[n + 1];
        var b = new long[n + 1];
        a[0] = 1L;
        b[0] = 0L;
        for (int i = 1; i <= n; ++i) {
            var a1 = i - k >= 1 ? a[i - k - 1] : 0L;
            var a2 = i >= d ? a[i - d] : 0L;
            var b2 = i >= d ? b[i - d] : 0L;
            a[i] = (a[i - 1] + (a[i - 1] + M - a1)) % M;
            b[i] = (b[i - 1] + (b[i - 1] + M - b2) + (a2 + M - a1)) % M;
        }
        Console.WriteLine((b[n] + M - b[n - 1]) % M);
    }
}
