using System;
using System.Collections.Generic;
using System.Linq;

class A43 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<string, int>();
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            int times = 0;
            a.TryGetValue(s, out times);
            a[s] = times + 1;
        }
        var m = a.Values.Max();
        Console.WriteLine(a.First(k => k.Value == m).Key);
    }
}
