using System;

class A776 {
    public static void Main() {
        var s = Console.ReadLine().Split();
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; ; ++i) {
            Console.WriteLine(string.Join(" ", s));
            if (i == n) break;
            var replace = Console.ReadLine().Split();
            if (s[0] == replace[0]) s[0] = replace[1];
            else s[1] = replace[1];
        }
    }
}
