using System;
using System.Linq;

class B785 {
    static int[][] ReadIntervals() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n][];
        for (int i = 0; i < n; ++i)
            a[i] = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        return a;
    }

    public static void Main() {
        var a1 = ReadIntervals();
        var a2 = ReadIntervals();
        var answer1 = a2.Max(k => k[0]) - a1.Min(k => k[1]);
        var answer2 = a1.Max(k => k[0]) - a2.Min(k => k[1]);
        Console.WriteLine(Math.Max(0, Math.Max(answer1, answer2)));
    }
}
