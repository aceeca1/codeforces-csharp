﻿using System;

class A318 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]);
        var k = long.Parse(line[1]);
        var numOdd = (n + 1) >> 1;
        Console.WriteLine(k <= numOdd ? k + k - 1 : (k - numOdd) << 1);
    }
}
