using System;

class A496 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int ans = int.MaxValue;
        for (int i = 0; i < a.Length - 2; ++i)
            ans = Math.Min(ans, a[i + 2] - a[i]);
        for (int i = 0; i < a.Length - 1; ++i)
            ans = Math.Max(ans, a[i + 1] - a[i]);
        Console.WriteLine(ans);
    }
}
