using System;
using System.Collections.Generic;
using System.Linq;

class B499 {
    static string Translate(Dictionary<string, string> a, string k) {
        var ak = a[k];
        return ak.Length < k.Length ? ak : k;
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = new Dictionary<string, string>();
        for (int i = 0; i < m; ++i) {
            var line1 = Console.ReadLine().Split();
            a[line1[0]] = line1[1];
        }
        line = Console.ReadLine().Split();
        Console.WriteLine(string.Join(" ", line.Select(k => Translate(a, k))));
    }
}
