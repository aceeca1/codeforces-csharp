using System;

class B734 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k2 = int.Parse(line[0]);
        var k3 = int.Parse(line[1]);
        var k5 = int.Parse(line[2]);
        var k6 = int.Parse(line[3]);
        var k256 = Math.Min(Math.Min(k2, k5), k6);
        k2 -= k256;
        var k32 = Math.Min(k2, k3);
        Console.WriteLine(k256 * 256 + k32 * 32);
    }
}
