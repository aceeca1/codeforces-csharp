using System;

class A75 {
    static int NoZero(int n) {
        return int.Parse(n.ToString().Replace("0", ""));
    }

    public static void Main() {
        var a1 = int.Parse(Console.ReadLine());
        var a2 = int.Parse(Console.ReadLine());
        var a3 = a1 + a2;
        Console.WriteLine(NoZero(a1) + NoZero(a2) == NoZero(a3) ? "YES" : "NO");
    }
}
