using System;

class A588 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var lowestPrice = int.MaxValue;
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a = int.Parse(line[0]);
            var p = int.Parse(line[1]);
            lowestPrice = Math.Min(lowestPrice, p);
            ans += a * lowestPrice;
        }
        Console.WriteLine(ans);
    }
}
