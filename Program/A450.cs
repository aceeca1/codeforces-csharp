using System;
using System.Linq;

class A450 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 0; i < n; ++i) a[i] = (a[i] + m - 1) / m;
        Console.WriteLine(Array.LastIndexOf(a, a.Max()) + 1);
    }
}
