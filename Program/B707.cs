using System;

class B707 {
    class Edge {
        public int S, T, C;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var k = int.Parse(line[2]);
        var e = new Edge[m];
        for (int i = 0; i < m; ++i) {
            line = Console.ReadLine().Split();
            e[i] = new Edge {
                S = int.Parse(line[0]) - 1,
                T = int.Parse(line[1]) - 1,
                C = int.Parse(line[2])
            };
        }
        var flour = new bool[n];
        if (k != 0) {
            line = Console.ReadLine().Split();
            foreach (var i in Array.ConvertAll(line, int.Parse))
                flour[i - 1] = true;
        }
        int answer = int.MaxValue;
        foreach (var i in e)
            if (flour[i.S] != flour[i.T])
                if (i.C < answer) answer = i.C;
        Console.WriteLine(answer == int.MaxValue ? -1 : answer);
    }
}
