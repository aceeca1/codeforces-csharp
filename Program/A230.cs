﻿using System;

class A230 {
    class Dragon {
        public int Strength, Bonus;
    }

    static bool KillAll(int s, Dragon[] a) {
        foreach (var i in a) {
            if (s <= i.Strength) return false;
            s += i.Bonus;
        }
        return true;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var s = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        var a = new Dragon[n];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            a[i] = new Dragon {
                Strength = int.Parse(line[0]),
                Bonus = int.Parse(line[1])
            };
        }
        Array.Sort(a, (k1, k2) => k1.Strength.CompareTo(k2.Strength));
        Console.WriteLine(KillAll(s, a) ? "YES" : "NO");
    }
}
