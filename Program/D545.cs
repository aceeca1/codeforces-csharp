using System;

class D545 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        int ans = 0, sum = 0;
        foreach (var i in a) 
            if (sum <= i) {
                sum += i;
                ++ans;
            }
        Console.WriteLine(ans);
    }
}
