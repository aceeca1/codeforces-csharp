using System;
using System.Linq;

class A110 {
    public static void Main() {
        var c = Console.ReadLine().Count(k => k == '4' || k == '7');
        Console.WriteLine(c == 4 || c == 7 ? "YES" : "NO");
    }
}
