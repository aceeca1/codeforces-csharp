using System;

class B570 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        int max = 0, arg = m;
        if ((m - 1) - 1 + 1 > max) { arg = m - 1; max = (m - 1) - 1 + 1; }
        if (n - (m + 1) + 1 > max) { arg = m + 1; max = n - (m + 1) + 1; }
        Console.WriteLine(arg);
    }
}
