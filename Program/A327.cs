﻿using System;
using System.Linq;

class A327 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        int ans = int.MinValue, c = 0;
        foreach (var i in a.Select(k => 1 - k - k)) {
            c = Math.Max(c, 0) + i;
            ans = Math.Max(ans, c);
        }
        Console.WriteLine(a.Sum() + ans);
    }
}
