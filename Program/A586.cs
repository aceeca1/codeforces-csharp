using System;
using System.Linq;

class A586 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 1; i < n - 1; ++i)
            if (a[i - 1] == 1 && a[i + 1] == 1) a[i] = 1;
        Console.WriteLine(a.Sum());
    }
}
