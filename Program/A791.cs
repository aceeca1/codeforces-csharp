using System;

class A791 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = double.Parse(line[0]);
        var b = double.Parse(line[1]);
        var ans = (int)Math.Floor(Math.Log(b / a) / Math.Log(1.5)) + 1;
        Console.WriteLine(ans);
    }
}
