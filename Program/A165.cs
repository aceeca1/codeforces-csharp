using System;
using System.Linq;

class A165 {
    class Point {
        public int[] X;
        public bool Central = true;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Point[n];
        for (int i = 0; i < n; ++i)
            a[i] = new Point {
                X = Array.ConvertAll(Console.ReadLine().Split(), int.Parse)
            };
        for (int i = 0; i < 2; ++i)
            foreach (var j in a.GroupBy(k => k.X[i])) {
                var min = j.Min(k => k.X[1 - i]);
                var max = j.Max(k => k.X[1 - i]);
                foreach (var k in j)
                    if (k.X[1 - i] == min || k.X[1 - i] == max)
                        k.Central = false;
            }
        Console.WriteLine(a.Count(k => k.Central));
    }
}
