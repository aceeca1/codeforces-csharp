using System;

class A701 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = new int[n];
        for (int i = 0; i < n; ++i) b[i] = i + 1;
        Array.Sort(a, b);
        for (int i = 0; i < n >> 1; ++i)
            Console.WriteLine($"{b[i]} {b[n - 1 - i]}");
    }
}
