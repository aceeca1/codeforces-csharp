﻿using System;
using System.Collections.Generic;

class A534 {
    static IEnumerable<int> Arrange(int n) {
        for (int i = 2; i <= n; i += 2) yield return i;
        for (int i = 1; i <= n; i += 2) yield return i;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        switch (n) {
            case 1:
            case 2: Console.WriteLine("1\n1"); break;
            case 3: Console.WriteLine("2\n1 3"); break;
            default:
                Console.WriteLine(n);
                Console.WriteLine(string.Join(" ", Arrange(n))); break;
        }
    }
}
