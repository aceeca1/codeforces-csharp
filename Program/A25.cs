﻿using System;
using System.Linq;

class A25 {
    static bool IsEven(int n) => (n & 1) == 0;

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(a.Count(IsEven) == 1
            ? Array.FindIndex(a, IsEven) + 1
            : Array.FindIndex(a, k => !IsEven(k)) + 1
        );
    }
}
