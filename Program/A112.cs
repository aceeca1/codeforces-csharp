using System;

class A112 {
    public static void Main() {
        var s1 = Console.ReadLine().ToLower();
        var s2 = Console.ReadLine().ToLower();
        Console.WriteLine(Math.Sign(s1.CompareTo(s2)));
    }
}
