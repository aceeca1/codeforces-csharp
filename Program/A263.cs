using System;

class A263 {
    public static void Main() {
        int c1 = -1, c2 = -1;
        for (int i1 = 0; i1 < 5; ++i1) {
            var line = Console.ReadLine().Split();
            for (int i2 = 0; i2 < 5; ++i2)
                if (line[i2] == "1") { c1 = i1; c2 = i2; }
        }
        var ans1 = Math.Abs(c1 - 2);
        var ans2 = Math.Abs(c2 - 2);
        Console.WriteLine(ans1 + ans2);
    }
}
