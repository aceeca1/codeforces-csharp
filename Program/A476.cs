using System;

class A476 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        int lb = (n + 1) >> 1;
        int ans = (lb + m - 1) / m * m;
        if (n < ans) Console.WriteLine(-1);
        else Console.WriteLine(ans);
    }
}
