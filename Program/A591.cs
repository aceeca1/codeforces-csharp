using System;
using System.Globalization;

class A591 {
    public static void Main() {
        var l = double.Parse(Console.ReadLine());
        var p = double.Parse(Console.ReadLine());
        var q = double.Parse(Console.ReadLine());
        var ans = l / (p + q) * p;
        Console.WriteLine(ans.ToString((CultureInfo.InvariantCulture)));
    }
}
