using System;
using System.Globalization;
using System.Linq;

class B200 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Console.ReadLine().Split().Select(k => int.Parse(k));
        Console.WriteLine(a.Average().ToString(CultureInfo.InvariantCulture));
    }
}
