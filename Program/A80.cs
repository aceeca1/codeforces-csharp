using System;

class A80 {
    static bool isPrime(int n) {
        for (int i = 2; i * i <= n; ++i)
            if (n % i == 0) return false;
        return true;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]) + 1;
        var m = int.Parse(line[1]);
        while (!isPrime(n)) ++n;
        Console.WriteLine(n == m ? "YES" : "NO");
    }
}
