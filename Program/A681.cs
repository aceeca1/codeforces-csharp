using System;

class A681 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        bool ans = false;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a1 = int.Parse(line[1]);
            var a2 = int.Parse(line[2]);
            if (2400 <= a1 && a1 < a2) ans = true;
        }
        Console.WriteLine(ans ? "YES" : "NO");
    }
}
