using System;

class A572 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var na = int.Parse(line[0]);
        var nb = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var ma = int.Parse(line[0]);
        var mb = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var b = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(a[ma - 1] < b[nb - mb] ? "YES" : "NO");
    }
}
