using System;

class A116 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int u = 0, ans = 0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var v1 = int.Parse(line[0]);
            var v2 = int.Parse(line[1]);
            u = u - v1 + v2;
            ans = Math.Max(ans, u);
        }
        Console.WriteLine(ans);
    }
}
