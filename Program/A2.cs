using System;
using System.Collections.Generic;
using System.Linq;

class A2 {
    class Arena {
        public string[] Name;
        public int[] Score;

        public string Winner() {
            var finalScore = new Dictionary<string, int>();
            for (int i = 0; i < Name.Length; ++i) {
                int v;
                finalScore.TryGetValue(Name[i], out v);
                finalScore[Name[i]] = v + Score[i];
            }
            var maxScore = finalScore.Values.Max();
            var currentScore = new Dictionary<string, int>();
            for (int i = 0; i < Name.Length; ++i) {
                int v;
                currentScore.TryGetValue(Name[i], out v);
                currentScore[Name[i]] = v + Score[i];
                if (maxScore <= v + Score[i])
                    if (finalScore[Name[i]] == maxScore) return Name[i];
            }
            return null;
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var name = new string[n];
        var score = new int[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            name[i] = line[0];
            score[i] = int.Parse(line[1]);
        }
        Console.WriteLine(new Arena { Name = name, Score = score }.Winner());
    }
}
