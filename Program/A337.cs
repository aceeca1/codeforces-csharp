﻿using System;

class A337 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        int ans = int.MaxValue;
        for (int i = 0; i + n - 1 < m; ++i)
            ans = Math.Min(ans, a[i + n - 1] - a[i]);
        Console.WriteLine(ans);
    }
}
