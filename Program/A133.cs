using System;
using System.Linq;

class A133 {
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(s.Any(k => {
            switch (k) {
                case 'H': case 'Q': case '9': return true;
                default: return false;
            }
        }) ? "YES" : "NO");
    }
}
