using System;
using System.Linq;

class A914 {
    static bool NotPerfectSquare(int n) {
        if (n < 0) return true;
        var a = (int)Math.Round(Math.Sqrt(n));
        return n != a * a;
    }
    
    public static void Main() {
        Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Console.WriteLine(a.Where(NotPerfectSquare).Max());
    }
}