using System;
using System.Linq;

class A160 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        var sum = a.Sum();
        var minTake = (sum >> 1) + 1;
        var take = 0;
        int k = a.Length - 1;
        while (take < minTake) take += a[k--];
        Console.WriteLine(a.Length - 1 - k);
    }
}
