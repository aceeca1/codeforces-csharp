using System;
using System.Text.RegularExpressions;

class A320 {
    static Regex Re = new Regex("^(1|14|144)+$");
    
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Re.IsMatch(s) ? "YES" : "NO");
    }
}
