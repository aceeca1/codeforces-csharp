using System;
using System.Collections.Generic;
using System.Linq;

class C546 {
    class Battle {
        public Queue<int>[] A;
        public int FightCount = 0;
    }

    class BattleResult: Exception {
        public int FightCount, Winner;
        public override string ToString() => $"{FightCount} {Winner}";
    }

    class BattleField: TortoiseHare<Battle> {
        public Battle Seed;

        protected override Battle Clone(Battle state) {
            return new Battle {
                A = new Queue<int>[] {
                    new Queue<int>(state.A[0]),
                    new Queue<int>(state.A[1])
                }
            };
        }

        protected override Battle GetSeed() => Seed;

        protected override bool IsEquivalent(Battle a1, Battle a2) =>
            a1.A[0].SequenceEqual(a2.A[0]) && a1.A[1].SequenceEqual(a2.A[1]);

        protected override void Next(Battle battle) {
            var head = new int[2];
            for (int i = 0; i < 2; ++i)
                if (battle.A[i].Count == 0)
                    throw new BattleResult {
                        FightCount = battle.FightCount,
                        Winner = 2 - i
                    };
                else head[i] = battle.A[i].Dequeue();
            ++battle.FightCount;
            var win = head[0] < head[1] ? 1 : 0;
            battle.A[win].Enqueue(head[1 - win]);
            battle.A[win].Enqueue(head[win]);
        }
    }

    public static void Main() {
        Console.ReadLine();
        var a1 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var a2 = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        try {
            new BattleField {
                Seed = new Battle {
                    A = new Queue<int>[2] {
                        new Queue<int>(a1.Skip(1)),
                        new Queue<int>(a2.Skip(1))
                    }
                }
            }.CycleLength();
            Console.WriteLine(-1);
        } catch (BattleResult result) {
            Console.WriteLine(result);
        }
    }

    // Snippet: TortoiseHare
    abstract class TortoiseHare<T> {
        protected abstract T GetSeed();
        protected abstract T Clone(T state);
        protected abstract bool IsEquivalent(T a1, T a2);
        protected abstract void Next(T state);

        public int CycleLength() {
            int n1 = 0, n2 = 1;
            T a1 = Clone(GetSeed()), a2 = Clone(a1);
            Next(a2);
            while (!IsEquivalent(a1, a2)) {
                if (n1 + n1 < n2) {
                    n1 = n2;
                    a1 = Clone(a2);
                }
                ++n2;
                Next(a2);
            }
            return n2 - n1;
        }
    }
}
