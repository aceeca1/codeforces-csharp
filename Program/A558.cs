using System;
using System.Linq;

class A558 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var x = new int[n + 1];
        var a = new int[n + 1];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            x[i] = int.Parse(line[0]);
            a[i] = int.Parse(line[1]);
        }
        Array.Sort(x, a);
        var k = Array.IndexOf(x, 0);
        var m = Math.Min(k, n - k);
        var answer = Enumerable.Range(k - m, m + m + 1).Sum(k1 => a[k1]);
        if (k < n - k) answer += a[k + m + 1];
        if (n - k < k) answer += a[k - m - 1];
        Console.WriteLine(answer);
    }
}
