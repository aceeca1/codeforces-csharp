using System;

class A352 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int a0 = 0, a5 = 0;
        foreach (var i in Console.ReadLine().Split())
            if (i[0] == '0') ++a0; else ++a5;
        a5 = a5 / 9 * 9;
        if (a0 == 0) Console.WriteLine(-1);
        else if (a5 == 0) Console.WriteLine(0);
        else Console.WriteLine(new string('5', a5) + new string('0', a0));
    }
}
