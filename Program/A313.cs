using System;
using System.Linq;

class A313 {
    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(new string[] {
            s, 
            s.Substring(0, s.Length - 1),
            s.Substring(0, s.Length - 2) + s[s.Length - 1]
        }.Max(int.Parse));
    }
}
