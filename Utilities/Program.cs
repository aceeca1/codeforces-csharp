﻿using System;

class Program {
    public static void Main() {
        var line = Console.ReadLine();
        Type.GetType(line).GetMethod("Main").Invoke(null, null);
    }
}
